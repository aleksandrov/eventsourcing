msbuild /t:Rebuild /p:Configuration=Release /m

rmdir output /s /q
mkdir output
mkdir output\lib

copy src\EventSourcing.EventDispatcher\bin\Release\EventSourcing*.* output\lib
copy src\EventSourcing.EventStore\bin\Release\EventSourcing*.* output\lib
copy src\EventSourcing.Logging\bin\Release\EventSourcing.* output\lib
copy src\EventSourcing.Misc.Mongo\bin\Release\EventSourcing.* output\lib
copy src\EventSourcing.Rebuild\bin\Release\EventSourcing.* output\lib
copy src\EventSourcing.Autofac\bin\Release\EventSourcing.* output\lib
copy src\EventSourcing.CommonDomain\bin\Release\EventSourcing.* output\lib

mkdir output\EventSourcing.Interfaces
mkdir output\EventSourcing.Interfaces\lib
mkdir output\EventSourcing.Interfaces\lib\net40
copy src\EventSourcing.Interfaces\bin\Release\EventSourcing.Interfaces.dll output\EventSourcing.Interfaces\lib\net40
copy src\EventSourcing.Interfaces\bin\Release\EventSourcing.Interfaces.nuspec output\EventSourcing.Interfaces

mkdir output\EventSourcing.CommonDomain
mkdir output\EventSourcing.CommonDomain\lib
mkdir output\EventSourcing.CommonDomain\lib\net40
copy src\EventSourcing.CommonDomain\bin\Release\EventSourcing.CommonDomain.dll output\EventSourcing.CommonDomain\lib\net40
copy src\EventSourcing.CommonDomain\bin\Release\EventSourcing.CommonDomain.nuspec output\EventSourcing.CommonDomain

mkdir output\EventSourcing.Cqrs.Messages
mkdir output\EventSourcing.Cqrs.Messages\lib
mkdir output\EventSourcing.Cqrs.Messages\lib\net40
copy src\EventSourcing.Cqrs.Messages\bin\Release\*.dll output\EventSourcing.Cqrs.Messages\lib\net40
copy src\EventSourcing.Cqrs.Messages\bin\Release\EventSourcing.Cqrs.Messages.nuspec output\EventSourcing.Cqrs.Messages


mkdir output\EventSourcing.Cqrs
mkdir output\EventSourcing.Cqrs\lib
mkdir output\EventSourcing.Cqrs\lib\net40
copy src\EventSourcing.Cqrs\bin\Release\EventSourcing.Cqrs.dll output\EventSourcing.Cqrs\lib\net40
copy src\EventSourcing.Cqrs\bin\Release\EventSourcing.Cqrs.nuspec output\EventSourcing.Cqrs

mkdir output\EventSourcing.Cqrs.Autofac
mkdir output\EventSourcing.Cqrs.Autofac\lib
mkdir output\EventSourcing.Cqrs.Autofac\lib\net40
copy src\EventSourcing.Cqrs.Autofac\bin\Release\EventSourcing.Cqrs.Autofac.dll output\EventSourcing.Cqrs.Autofac\lib\net40
copy src\EventSourcing.Cqrs.Autofac\bin\Release\EventSourcing.Cqrs.Autofac.nuspec output\EventSourcing.Cqrs.Autofac

mkdir output\EventSourcing.EventDispatcher
mkdir output\EventSourcing.EventDispatcher\lib
mkdir output\EventSourcing.EventDispatcher\lib\net40
copy src\EventSourcing.EventDispatcher\bin\Release\EventSourcing.EventDispatcher.dll output\EventSourcing.EventDispatcher\lib\net40
copy src\EventSourcing.EventDispatcher\bin\Release\EventSourcing.EventDispatcher.nuspec output\EventSourcing.EventDispatcher

mkdir output\EventSourcing.EventStore
mkdir output\EventSourcing.EventStore\lib
mkdir output\EventSourcing.EventStore\lib\net40
copy src\EventSourcing.EventStore\bin\Release\EventSourcing.EventStore.dll output\EventSourcing.EventStore\lib\net40
copy src\EventSourcing.EventStore\bin\Release\EventSourcing.EventStore.nuspec output\EventSourcing.EventStore

mkdir output\EventSourcing.Rebuild.Messages
mkdir output\EventSourcing.Rebuild.Messages\lib
mkdir output\EventSourcing.Rebuild.Messages\lib\net40
copy src\EventSourcing.Rebuild.Messages\bin\Release\EventSourcing.Rebuild.Messages.dll output\EventSourcing.Rebuild.Messages\lib\net40
copy src\EventSourcing.Rebuild.Messages\bin\Release\EventSourcing.Rebuild.Messages.nuspec output\EventSourcing.Rebuild.Messages

mkdir output\EventSourcing.Rebuild
mkdir output\EventSourcing.Rebuild\lib
mkdir output\EventSourcing.Rebuild\lib\net40
copy src\EventSourcing.Rebuild\bin\Release\EventSourcing.Rebuild.dll output\EventSourcing.Rebuild\lib\net40
copy src\EventSourcing.Rebuild\bin\Release\EventSourcing.Rebuild.nuspec output\EventSourcing.Rebuild

mkdir output\EventSourcing.Logging
mkdir output\EventSourcing.Logging\lib
mkdir output\EventSourcing.Logging\lib\net40
copy src\EventSourcing.Logging\bin\Release\EventSourcing.Logging.dll output\EventSourcing.Logging\lib\net40
copy src\EventSourcing.Logging\bin\Release\EventSourcing.Logging.nuspec output\EventSourcing.Logging

mkdir output\EventSourcing.Misc.Mongo
mkdir output\EventSourcing.Misc.Mongo\lib
mkdir output\EventSourcing.Misc.Mongo\lib\net40
copy src\EventSourcing.Misc.Mongo\bin\Release\EventSourcing.Misc.Mongo.dll output\EventSourcing.Misc.Mongo\lib\net40
copy src\EventSourcing.Misc.Mongo\bin\Release\EventSourcing.Misc.Mongo.nuspec output\EventSourcing.Misc.Mongo




rem copy src\EventSourcing.nuspec output\

rem bin\ilmerge-bin\ilmerge.exe /targetplatform:"v4,C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.5" /target:library /out:output\EventSourcing.dll output\EventSourcing.Rebuild.dll output\EventSourcing.Cqrs.dll output\EventSourcing.Cqrs.Messages.dll output\EventSourcing.EventDispatcher.dll output\EventSourcing.EventStore.dll output\EventSourcing.Logging.dll output\EventSourcing.Misc.Mongo.dll 