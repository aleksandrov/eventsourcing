﻿using EventSourcing.Cqrs.Messages.Events.Runtime;
using EventSourcing.Cqrs.Services;
using EventSourcing.Interfaces.Services;
using Machine.Fakes;
using Machine.Specifications;
using NSubstitute;

namespace EventSourcing.Cqrs.Specs.Services
{
    [Subject(typeof(RuntimeManager))]
    class when_stopping_the_running_system : RuntimeManagerSpecs
    {
        Establish context = () => Subject.Start();

        private Because of = () => Subject.Stop();

        private It should_raise_SystemStopping_event = () => The<IEventPublisher>().Received().Publish(Arg.Any<SystemStoppingEvent>());
    }

    [Subject(typeof(RuntimeManager))]
    class when_stopping_the_system_which_is_already_in_stopping_state : RuntimeManagerSpecs
    {
        Establish context = () =>
        {
            Subject.Start();
            Subject.Stop();
        };

        private Because of = () => Subject.Stop();

        private It should_not_raise_SystemStopping_event = () => The<IEventPublisher>().Received(1).Publish(Arg.Any<SystemStoppingEvent>());
    }

    class RuntimeManagerSpecs : WithSubject<RuntimeManager>
    {
        Establish context = () =>
        {
            
        };
    }
}
