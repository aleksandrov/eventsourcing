﻿using System;

namespace EventSourcing.Cqrs.Messages.Commands
{
    public class Command : ICommand
    {
        public Guid CorrelationId { get; set; }
        public Guid Id { get; set; }
    }
}
