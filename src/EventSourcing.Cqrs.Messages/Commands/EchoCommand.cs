﻿using System;

namespace EventSourcing.Cqrs.Messages.Commands
{
    public class EchoCommand : ICommand
    {
        public string Message { get; set; }
        public Guid CorrelationId { get; set; }
        public Guid Id { get; set; }
    }
}
