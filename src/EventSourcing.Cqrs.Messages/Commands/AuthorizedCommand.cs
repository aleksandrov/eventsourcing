﻿using System;

namespace EventSourcing.Cqrs.Messages.Commands
{
    public abstract class AuthorizedCommand : ICommand
    {
        public Guid AuthorizedUserId { get; set; }
        public Guid CorrelationId { get; set; }

        protected AuthorizedCommand(Guid authorizedUserId)
        {
            AuthorizedUserId = authorizedUserId;
        }

        public Guid Id { get; set; }
    }
}
