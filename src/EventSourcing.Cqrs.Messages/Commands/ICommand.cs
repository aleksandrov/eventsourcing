﻿using System;

namespace EventSourcing.Cqrs.Messages.Commands
{
	public interface ICommand : IMessage
	{
		Guid Id { get; set; }
	}
}