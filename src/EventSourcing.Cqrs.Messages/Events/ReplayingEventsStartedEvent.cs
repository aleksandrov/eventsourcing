﻿using System;

namespace EventSourcing.Cqrs.Messages.Events
{
	public class ReplayingEventsStartedEvent : IEvent
	{
	    public DateTime Created { get; set; }
	    public int Version { get; set; }
	    public Guid CorrelationId { get; set; }
	}
}