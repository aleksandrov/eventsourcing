﻿using System;
using EventSourcing.Cqrs.Messages.Commands;

namespace EventSourcing.Cqrs.Messages.Events
{
	public class CommandDispatchFailedEvent : Event
	{
	    public string SagaType { get; set; }
	    public ICommand Command { get; set; }
	    public string ErrorMessage { get; set; }
	    public string StackTrace { get; set; }

	    public CommandDispatchFailedEvent(Guid correlationId, string sagaType, ICommand command, string errorMessage, string stackTrace = null)
	    {
	        CorrelationId = correlationId;
	        SagaType = sagaType;
	        Command = command;
	        ErrorMessage = errorMessage;
	        StackTrace = stackTrace;
	    }
	}
}
