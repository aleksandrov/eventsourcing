﻿using System;

namespace EventSourcing.Cqrs.Messages.Events
{
	public interface IEvent : IMessage
	{
        DateTime Created { get; set; }

		int Version { get; set; }
	}
}