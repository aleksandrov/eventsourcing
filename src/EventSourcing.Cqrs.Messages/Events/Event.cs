﻿using System;

namespace EventSourcing.Cqrs.Messages.Events
{
    public abstract class Event : IEvent
    {
        #region IEvent Members

        public DateTime Created { get; set; }

        public int Version { get; set; }

        #endregion

        public Guid CorrelationId { get; set; }
    }
}
