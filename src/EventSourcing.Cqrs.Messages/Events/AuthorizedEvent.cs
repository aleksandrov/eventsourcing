﻿using System;

namespace EventSourcing.Cqrs.Messages.Events
{
    public class AuthorizedEvent : Event
    {
        public Guid AuthorizedUserId { get; set; }
    }
}
