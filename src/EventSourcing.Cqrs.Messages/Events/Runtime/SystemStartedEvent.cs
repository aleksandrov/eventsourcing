﻿using System;

namespace EventSourcing.Cqrs.Messages.Events.Runtime
{
	public class SystemStartedEvent : IEvent
	{
	    public DateTime Created { get; set; }
	    public int Version { get; set; }
	    public Guid CorrelationId { get; set; }
	}
}