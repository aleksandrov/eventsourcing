﻿using System;

namespace EventSourcing.Cqrs.Messages
{
	public interface IMessage
	{
        Guid CorrelationId { get; set; }	
	}
}