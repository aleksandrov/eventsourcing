﻿using System;
using System.Runtime.Serialization;

namespace EventSourcing.Cqrs.Messages
{

    [DataContract]
    public class ExecutionResult
    {
        [DataMember(IsRequired = true, Name = "id")]
        public Guid Id { get; set; }

        [DataMember(IsRequired = true, Name = "version")]
        public int Version { get; set; }

        [DataMember(IsRequired = true, Name = "message")]
        public string Message { get; set; }

        public static ExecutionResult Empty
        {
            get
            {
                return new ExecutionResult();
            }
        }
    }
}
