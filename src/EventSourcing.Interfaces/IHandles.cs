﻿using EventSourcing.Cqrs.Messages;

namespace EventSourcing.Interfaces
{
    public interface IHandles<TMessage> where TMessage : IMessage
    {
        ExecutionResult Handle(TMessage message);
    }
}
