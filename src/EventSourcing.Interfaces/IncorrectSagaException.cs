﻿using System;

namespace EventSourcing.Interfaces
{
    public class IncorrectSagaException : Exception
    {
        public string ExpectedType { get; set; }
        public string ActualType { get; set; }

        public IncorrectSagaException(string expectedType, string actualType)
        {
            ExpectedType = expectedType;
            ActualType = actualType;
        }
    }
}
