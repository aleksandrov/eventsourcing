﻿using System.Collections.Generic;
using EventSourcing.Cqrs.Messages;
using EventSourcing.Cqrs.Messages.Events;

namespace EventSourcing.Interfaces
{
    public interface IHeaderProcessor
    {
        IDictionary<string, object> GetHeaders(IMessage message);

        void UpdateEvent(IEvent message, IDictionary<string, object> headers);
    }
}
