﻿namespace EventSourcing.Interfaces
{
	public interface IProjection
	{
	    void OnRebuildStarted();
	    void OnRebuildFinished();
	}
}