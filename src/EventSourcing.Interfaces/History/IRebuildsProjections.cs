﻿using System.Collections.Generic;
using Autofac;

namespace EventSourcing.Interfaces.History
{
    public interface IRebuildsProjections
    {
        //void BeginRebuild();

        //void EndRebuild();

        //void CleanupProjections(string[] projectionNames, bool revertSelection, ILifetimeScope scope);

        //void RebuildProjections(IEvent @event, string[] projectionNames, bool revertSelection, ILifetimeScope scope);

        IEnumerable<IRebuildable> GetRebuildableProjections(IEnumerable<string> projectionNames, bool revertSelection);

        //void RebuildProjectionsAsync(IEvent @event, string[] projectionNames, bool revertSelection);

        //bool IsProjectionsAreBusy();
    }
}
