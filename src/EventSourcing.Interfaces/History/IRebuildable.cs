﻿using Autofac;
using EventSourcing.Cqrs.Messages.Events;

namespace EventSourcing.Interfaces.History
{
    public interface IRebuildable
    {
        //void OnRebuildStarted(ILifetimeScope scope);

        //void OnRebuildFinished(ILifetimeScope scope);

        void Rebuild(IEvent @event, ILifetimeScope scope);

        string Name { get; }

    }
}
