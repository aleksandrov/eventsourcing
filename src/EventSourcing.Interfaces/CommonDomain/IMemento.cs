using System;

namespace EventSourcing.Interfaces.CommonDomain
{
    public interface IMemento
	{
		Guid Id { get; set; }

		int Version { get; set; }
	}
}