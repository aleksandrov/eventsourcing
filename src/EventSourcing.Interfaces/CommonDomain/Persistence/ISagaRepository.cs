using System;
using System.Collections.Generic;

namespace EventSourcing.Interfaces.CommonDomain.Persistence
{
    public interface ISagaRepository
	{
		ISaga GetById<TSaga>(Guid sagaId) where TSaga : class, ISaga;

		void Save(ISaga saga, Guid commitId, Action<IDictionary<string, object>> updateHeaders);
	}
}