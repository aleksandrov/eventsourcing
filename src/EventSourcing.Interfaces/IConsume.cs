﻿using EventSourcing.Cqrs.Messages;

namespace EventSourcing.Interfaces
{
    public interface IConsume<TMessage> where TMessage : IMessage
    {
        void Consume(TMessage message);
    }
}
