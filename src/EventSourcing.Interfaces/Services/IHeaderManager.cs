﻿using System.Collections.Generic;
using EventSourcing.Cqrs.Messages;
using EventSourcing.Cqrs.Messages.Events;

namespace EventSourcing.Interfaces.Services
{
    public interface IHeaderManager
    {
        void UpdateHeaders(IDictionary<string, object> headers, IMessage message);

        void UpdateEvent(IEvent message, IDictionary<string, object> headers);
    }
}
