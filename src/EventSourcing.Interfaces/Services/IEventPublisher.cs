﻿using System.Collections.Generic;
using EventSourcing.Cqrs.Messages.Events;

namespace EventSourcing.Interfaces.Services
{
	public interface IEventPublisher
	{
		void Publish(IEvent @event);

	    void Publish(IEnumerable<IEvent> events);
	}
}