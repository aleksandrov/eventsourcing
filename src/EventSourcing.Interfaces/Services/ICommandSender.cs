﻿using EventSourcing.Cqrs.Messages;
using EventSourcing.Cqrs.Messages.Commands;

namespace EventSourcing.Interfaces.Services
{
	public interface ICommandSender
	{
        ExecutionResult Send(ICommand command);
	}
}