﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EventSourcing.Cqrs.Messages;
using EventSourcing.Cqrs.Messages.Commands;
using EventSourcing.Cqrs.Messages.Events;
using EventSourcing.Interfaces.Services;
using NEventStore;
using NEventStore.Dispatcher;

namespace EventSourcing.EventStore
{
    public class CommitDispatcher : IDispatchCommits
    {
        public const string SAGA_TYPE_HEADER = "SagaType";
        public const string UNDISPATCHED_MESSAGES_HEADER_PREFIX = "UndispatchedMessage";

        private readonly ICommandSender commandSender;
        private readonly IEventPublisher eventPublisher;
        private readonly ILogger logger;
        private readonly IHeaderManager headerManager;
        private readonly object lockObject = new object();

        public CommitDispatcher(ICommandSender commandSender, IEventPublisher eventPublisher, ILogger logger, IHeaderManager headerManager)
        {
            this.commandSender = commandSender;
            this.eventPublisher = eventPublisher;
            this.logger = logger;
            this.headerManager = headerManager;
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize((object)this);
        }

        protected virtual void Dispose(bool disposing)
        {
        }

        public void Dispatch(ICommit commit)
        {
            if (commit.Headers.Keys.Any(s => s == SAGA_TYPE_HEADER))
            {
                IList<object> messages = commit.Headers.Where(x => x.Key.StartsWith(UNDISPATCHED_MESSAGES_HEADER_PREFIX)).Select(x => x.Value).ToList();
                if (messages.Count > 0)
                {
                    Task.Factory.StartNew(() =>
                    {
                        var events = messages.OfType<IEvent>().Select(e => PopulateEventFields(commit, e));
                        eventPublisher.Publish(events);

                        foreach (var message in messages.Cast<IMessage>())
                        {
                            if (message is ICommand)
                            {
                                DispatchCommand(commit, message as ICommand);
                            }
                        }
                    });
                }
            }
            else
            {
                logger.DebugFormat("Dispatching commit: {0}, Checkpoint: {1}, Bucket: {2}", commit.CommitId, commit.CheckpointToken, commit.BucketId);
                var events = commit.Events.Select(e => e.Body).OfType<IEvent>().Select(e => PopulateEventFields(commit, e));
                eventPublisher.Publish(events);
            }
        }

        private IEvent PopulateEventFields(ICommit commit, IEvent @event)
        {
            headerManager.UpdateEvent(@event, commit.Headers);
            return @event;
        }

        private void DispatchCommand(ICommit commit, ICommand command)
        {
            try
            {
                commandSender.Send(command);
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Unable to send command {0}: {1}", command.GetType().Name, ex.Message);
                eventPublisher.Publish(new CommandDispatchFailedEvent(new Guid(commit.StreamId), commit.Headers[SAGA_TYPE_HEADER].ToString(), command, ex.Message, ex.StackTrace));
            }

        }


    }
}
