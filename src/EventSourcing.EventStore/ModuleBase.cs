﻿using Autofac;
using Autofac.Core;
using EventSourcing.CommonDomain;
using EventSourcing.CommonDomain.Core;
using EventSourcing.EventStore.CommonDomain.Persistence.EventStore;
using EventSourcing.EventStore.Factories;
using EventSourcing.EventStore.Logger;
using EventSourcing.EventStore.Repositories;
using EventSourcing.Interfaces.CommonDomain.Persistence;
using NEventStore;
using NEventStore.Dispatcher;

namespace EventSourcing.EventStore
{
    public class ModuleBase : Module
    {
        public string StoreName { get; private set; }
        protected string BucketName { get; private set; }

        protected ModuleBase(string storeName, string bucketName)
        {
            StoreName = storeName;
            BucketName = bucketName;
        }

        

        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<Factory>().AsImplementedInterfaces();
            builder.RegisterType<CommitDispatcher>().As<IDispatchCommits>();
            builder.RegisterType<LoggerAdapter>().As<LoggerAdapter>();
            builder.RegisterType<LoggerAdapterFactory>().As<LoggerAdapterFactory>();

            // common domain stuff
            builder.RegisterType<ConflictDetector>().As<IDetectConflicts>().SingleInstance();

            builder.RegisterType<BucketAwareEventStoreRepository>().Named<IRepository>(BucketName)
                .WithParameter(new TypedParameter(typeof(string), BucketName))
                .WithParameter(ResolvedParameter.ForNamed<IStoreEvents>(StoreName));

            // TODO: implement "Bucket Aware" SagaEventStoreRepository
            builder.RegisterType<SagaEventStoreRepository>().Named<ISagaRepository>(BucketName)
                .WithParameter(ResolvedParameter.ForNamed<IStoreEvents>(StoreName));

            // EventStore itself
            builder.Register(ctx => CreateWireup(ctx).Build()).Named<IStoreEvents>(StoreName).SingleInstance();

        }

        protected virtual Wireup CreateWireup(IComponentContext ctx)
        {
            var logFactory = ctx.Resolve<LoggerAdapterFactory>();

            return Wireup.Init()
                .LogTo(logFactory.CreateLogger)
                .UsingSynchronousDispatchScheduler(ctx.Resolve<IDispatchCommits>());
        }
    }
}
