﻿using System;
using System.Collections.Generic;
using EventSourcing.CommonDomain;
using EventSourcing.EventStore.CommonDomain;
using EventSourcing.EventStore.CommonDomain.Persistence;
using EventSourcing.EventStore.CommonDomain.Persistence.EventStore;
using EventSourcing.Interfaces.CommonDomain;
using EventSourcing.Interfaces.Services;
using NEventStore;

namespace EventSourcing.EventStore.Repositories
{
    class BucketAwareEventStoreRepository : EventStoreRepository
    {
        private readonly string bucket;

        public BucketAwareEventStoreRepository(string bucket, IStoreEvents eventStore, IConstructAggregates factory, IDetectConflicts conflictDetector, IHeaderManager headerManager)
            : base(eventStore, factory, conflictDetector, headerManager)
        {
            this.bucket = bucket;
        }

        public override TAggregate GetById<TAggregate>(Guid id)
        {
            return GetById<TAggregate>(bucket, id);
        }

        public override TAggregate GetById<TAggregate>(Guid id, int versionToLoad)
        {
            return GetById<TAggregate>(bucket, id, versionToLoad);
        }

        public override void Save(IAggregate aggregate, Guid commitId, Action<IDictionary<string, object>> updateHeaders)
        {
            Save(bucket, aggregate, commitId, updateHeaders);
        }

    }
}
