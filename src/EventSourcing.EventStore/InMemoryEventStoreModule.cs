﻿using Autofac;
using NEventStore;
using NEventStore.Dispatcher;

namespace EventSourcing.EventStore
{
    public class InMemoryEventStoreModule : ModuleBase
	{
        public InMemoryEventStoreModule(string storeName, string bucketName) : base(storeName, bucketName)
        {
        }

        protected override Wireup CreateWireup(IComponentContext ctx)
		{
			return base.CreateWireup(ctx)
                .UsingInMemoryPersistence()
                .InitializeStorageEngine()
                .UsingAsynchronousDispatchScheduler(ctx.Resolve<IDispatchCommits>());
		}
	}
}