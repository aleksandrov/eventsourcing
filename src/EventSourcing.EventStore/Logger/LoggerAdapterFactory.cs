﻿using System;
using Autofac;
using Autofac.Core;
using EventSourcing.Interfaces.Services;
using NEventStore.Logging;

namespace EventSourcing.EventStore.Logger
{
	public class LoggerAdapterFactory
	{
		#region Delegates

		public delegate LoggerAdapter LoggerFactory(string name);

		#endregion

		private readonly ILifetimeScope scope;

		public LoggerAdapterFactory(ILifetimeScope scope)
		{
			if (scope == null) throw new ArgumentNullException("scope");
			this.scope = scope;
		}

		public ILog CreateLogger(Type t)
		{
			if (t == null) throw new ArgumentNullException("t");

		    try
		    {
		        return
		            scope.Resolve<LoggerAdapter>(
		                new ResolvedParameter(
		                    (pi, ctx) => pi.ParameterType == typeof (ILogger),
		                    (pi, ctx) => ctx.Resolve<ILogger>(new NamedParameter("name", t.FullName))));
		    }
            catch (ObjectDisposedException)
		    {
                // workarond for EventStore. EventStore tries to resolve loger during Dispose() which is not possible to do using Autofac,
                // because LifetimeScope is already disposed
		        return new NullLogger();
		    }
		}
	}
}