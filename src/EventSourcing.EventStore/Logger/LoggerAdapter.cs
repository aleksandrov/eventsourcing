﻿using System;
using EventSourcing.Interfaces.Services;
using NEventStore.Logging;

namespace EventSourcing.EventStore.Logger
{
	public class LoggerAdapter : ILog
	{
		private readonly ILogger _logger;

		public LoggerAdapter(ILogger logger)
		{
			if (logger == null) throw new ArgumentNullException("logger");
			_logger = logger;
		}

		#region Implementation of ILog

		/// <summary>
		/// 	Logs the most detailed level of diagnostic information.
		/// </summary>
		/// <param name="message"> The diagnostic message to be logged. </param>
		/// <param name="values"> All parameter to be formatted into the message, if any. </param>
		public void Verbose(string message, params object[] values)
		{
			_logger.DebugFormat(message, values);
		}

		/// <summary>
		/// 	Logs the debug-level diagnostic information.
		/// </summary>
		/// <param name="message"> The diagnostic message to be logged. </param>
		/// <param name="values"> All parameter to be formatted into the message, if any. </param>
		public void Debug(string message, params object[] values)
		{
			_logger.DebugFormat(message, values);
		}

		/// <summary>
		/// 	Logs important runtime diagnostic information.
		/// </summary>
		/// <param name="message"> The diagnostic message to be logged. </param>
		/// <param name="values"> All parameter to be formatted into the message, if any. </param>
		public void Info(string message, params object[] values)
		{
			_logger.InfoFormat(message, values);
		}

		/// <summary>
		/// 	Logs diagnostic issues to which attention should be paid.
		/// </summary>
		/// <param name="message"> The diagnostic message to be logged. </param>
		/// <param name="values"> All parameter to be formatted into the message, if any. </param>
		public void Warn(string message, params object[] values)
		{
			_logger.WarnFormat(message, values);
		}

		/// <summary>
		/// 	Logs application and infrastructure-level errors.
		/// </summary>
		/// <param name="message"> The diagnostic message to be logged. </param>
		/// <param name="values"> All parameter to be formatted into the message, if any. </param>
		public void Error(string message, params object[] values)
		{
			_logger.ErrorFormat(message, values);
		}

		/// <summary>
		/// 	Logs fatal errors which result in process termination.
		/// </summary>
		/// <param name="message"> The diagnostic message to be logged. </param>
		/// <param name="values"> All parameter to be formatted into the message, if any. </param>
		public void Fatal(string message, params object[] values)
		{
			_logger.FatalFormat(message, values);
		}

		#endregion
	}
}