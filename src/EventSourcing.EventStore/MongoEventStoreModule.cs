﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Autofac;
using MongoDB.Bson.Serialization;
using NEventStore;
using NEventStore.Serialization;

namespace EventSourcing.EventStore
{
    public class MongoEventStoreModule : ModuleBase
	{
        private readonly string connectionString;
        private readonly IEnumerable<Assembly> messageAssemblies;

        #region Startup

		public class StartupScript : IStartable
		{
		    private readonly IEnumerable<Assembly> messageAssemblies;

		    public StartupScript(IEnumerable<Assembly> messageAssemblies)
            {
                this.messageAssemblies = messageAssemblies;
            }

		    public void Start()
			{
				InitMapping();
			}

			private void InitMapping()
			{
                var types = messageAssemblies.SelectMany(a => a.GetTypes()).ToList();
                foreach (var type in types)
				{
					if (type.IsInterface) continue;
					if (type.IsAbstract) continue;

					if (!BsonClassMap.IsClassMapRegistered(type))
					{
						BsonClassMap.RegisterClassMap(new RuntimeClassMap(type));
					}
				}
			}
		}

		#endregion

		#region Custom ClassMap

		private class RuntimeClassMap : BsonClassMap
		{
			public RuntimeClassMap(Type classType)
				: base(classType)
			{
				AutoMap();
				SetIgnoreExtraElements(true);
			}
		}

		#endregion

		public MongoEventStoreModule(string storeName, string bucketName, string connectionString, IEnumerable<Assembly> messageAssemblies) : base(storeName, bucketName)
		{
		    
		    if (bucketName == null) throw new ArgumentNullException("bucketName");
		    if (connectionString == null) throw new ArgumentNullException("connectionString");
		    this.connectionString = connectionString;
		    this.messageAssemblies = messageAssemblies;

		}

        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.Register(ctx => new StartupScript(messageAssemblies)).As<IStartable>();
        }

		protected override Wireup CreateWireup(IComponentContext ctx)
		{
            return base.CreateWireup(ctx).UsingMongoPersistence(() => connectionString, new DocumentObjectSerializer()).InitializeStorageEngine();
		}
	}
}