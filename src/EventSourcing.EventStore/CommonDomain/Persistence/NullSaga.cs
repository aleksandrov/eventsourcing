﻿using System;
using System.Collections;
using EventSourcing.Interfaces.CommonDomain;

namespace EventSourcing.EventStore.CommonDomain.Persistence
{
    class NullSaga : ISaga
    {
        public Guid Id { get; private set; }
        public int Version { get; private set; }
        public void Transition(object message)
        {
            
        }

        public ICollection GetUncommittedEvents()
        {
            return new object[0];
        }

        public void ClearUncommittedEvents()
        {
            
        }

        public ICollection GetUndispatchedMessages()
        {
            return new object[0];
        }

        public void ClearUndispatchedMessages()
        {
            
        }
    }
}
