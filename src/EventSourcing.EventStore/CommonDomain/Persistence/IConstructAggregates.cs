using System;
using System.Collections.Generic;
using EventSourcing.Interfaces.CommonDomain;

namespace EventSourcing.EventStore.CommonDomain.Persistence
{
    public interface IConstructAggregates
	{
        IAggregate Build(Type type, Guid id, IMemento snapshot, IDictionary<string, object> headers);
	}
}