﻿using System;
using EventSourcing.Interfaces.CommonDomain;

namespace EventSourcing.EventStore.CommonDomain.Persistence
{
    public interface IConstructSagas
    {
        TSaga Build<TSaga>(Guid id) where TSaga : class, ISaga;
    }
}
