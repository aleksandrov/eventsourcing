﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using Autofac.Core;
using EventSourcing.EventStore.CommonDomain.Persistence;
using EventSourcing.Interfaces.CommonDomain;

namespace EventSourcing.EventStore.Factories
{
    class Factory : IConstructAggregates, IConstructSagas
    {
        private const string AGGREGATE_TYPE_HEADER = "AggregateType";

        public delegate TSaga SagaFactory<TSaga>(Guid id) where TSaga : ISaga;

        private readonly ILifetimeScope scope;

        public Factory(ILifetimeScope scope)
        {
            if (scope == null) throw new ArgumentNullException("scope");
            this.scope = scope;
        }


        public IAggregate Build(Type type, Guid id, IMemento snapshot, IDictionary<string, object> headers)
        {
            var concreteType = type;
            if (headers != null)
            {
                if (headers.ContainsKey(AGGREGATE_TYPE_HEADER))
                {
                    // there is more easier way to get type of aggegate (currently does not work if aggregate live in another assembly):
                    // concreteType = Type.GetType((string)headers[AGGREGATE_TYPE_HEADER]);

                    var registration = scope.ComponentRegistry.Registrations.Single(r => r.Activator.LimitType.FullName == (string)headers[AGGREGATE_TYPE_HEADER]);
                    concreteType = registration.Activator.LimitType;
                }
            }


            object result;

            try
            {
                result = scope.Resolve(concreteType);
            }
            catch (DependencyResolutionException ex)
            {
                result = scope.Resolve(concreteType, new NamedParameter("id", id));
            }

            return (IAggregate)result;
        }


        public TSaga Build<TSaga>(Guid id) where TSaga : class, ISaga
        {
            object result;
            try
            {
                result = scope.Resolve(typeof (TSaga));
            }
            catch (DependencyResolutionException ex)
            {
                result = scope.Resolve(typeof (TSaga), new NamedParameter("id", id));
            }
            return (TSaga) result;
        }
    }
}
