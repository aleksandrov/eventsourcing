﻿using EventSourcing.Interfaces.Services;

namespace EventSourcing.Logging
{
    class GenericLog4NetLogger<T> : Log4NetLogger, ILogger<T>
    {
        public GenericLog4NetLogger() : base(typeof(T).Name)
        {
            
        }
    }
}
