﻿using System;
using System.Linq;
using Autofac;
using Autofac.Core;
using EventSourcing.Interfaces.Services;
using log4net.Config;

namespace EventSourcing.Logging
{
	public class Log4NetModule : Module
	{
        protected override void AttachToComponentRegistration(Autofac.Core.IComponentRegistry componentRegistry, Autofac.Core.IComponentRegistration registration)
        {
            base.AttachToComponentRegistration(componentRegistry, registration);

            registration.Preparing += OnPreparing;
        }

        void OnPreparing(object sender, Autofac.Core.PreparingEventArgs e)
        {
            var t = e.Component.Activator.LimitType;
            e.Parameters = e.Parameters.Union(new[] 
            { 
                new ResolvedParameter((p, i) => p.ParameterType == typeof(ILogger), (p, i) =>
                {
                    Type result = t;
                    if (t.IsGenericType)
                    {
                        result = t.GetGenericTypeDefinition();
                    }
                    return e.Context.Resolve<ILogger>(new NamedParameter("name", result.FullName)); // new Log4NetLogger(result.FullName);
                })
            });
        }

		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);

			XmlConfigurator.Configure();

			builder.RegisterType<Log4NetLogger>().As<ILogger>();
		    builder.RegisterGeneric(typeof (GenericLog4NetLogger<>)).As(typeof (ILogger<>));
		}
	}
}
