﻿using Autofac;
using EventSourcing.Rebuild.Disruptor;
using EventSourcing.Rebuild.Entities;

namespace EventSourcing.Rebuild.Services
{
    interface IRebuildService
    {
        RebuildContext CreateContext(ILifetimeScope contextScope, string[] buckets, string[] projectionNames, bool revertSelection, bool writeCheckpoint);

        RebuildResult Rebuild(string[] buckets, string[] projectionNames, bool revertSelection);
    }
}
