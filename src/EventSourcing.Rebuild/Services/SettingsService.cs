﻿namespace EventSourcing.Rebuild.Services
{
    class SettingsService : IRemoteUpdateSettings
    {
        public bool IgnoreUnkownMessages { get; private set; }

        public int PullInterval { get; private set; }

        public string RemoteEsConnectionString { get; private set; }

        public SettingsService(bool ignoreUnkownMessages, int pullInterval, string remoteEsConnectionString)
        {
            IgnoreUnkownMessages = ignoreUnkownMessages;
            PullInterval = pullInterval;
            RemoteEsConnectionString = remoteEsConnectionString;
        }
    }
}
