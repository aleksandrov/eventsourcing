﻿using System.Diagnostics;
using Autofac;
using EventSourcing.Interfaces.History;
using EventSourcing.Interfaces.Services;
using EventSourcing.Rebuild.Disruptor;
using EventSourcing.Rebuild.Entities;
using NEventStore;
using NEventStore.Serialization;

namespace EventSourcing.Rebuild.Services
{
    class RebuildService : IRebuildService
    {
        private readonly IRemoteUpdateSettings settings;
        private readonly IWaitStrategyFactory waitStrategyFactory;
        private readonly ILifetimeScope scope;
        private readonly IRebuildsProjections rebuilder;
        private readonly ILogger logger;
        private readonly int ringSize;

        public RebuildService(IRemoteUpdateSettings settings, IWaitStrategyFactory waitStrategyFactory, ILifetimeScope scope, IRebuildsProjections rebuilder, ILogger logger, int ringSize = 1024)
        {
            this.settings = settings;
            this.waitStrategyFactory = waitStrategyFactory;
            this.scope = scope;
            this.rebuilder = rebuilder;
            this.logger = logger;
            this.ringSize = ringSize;

            
        }

        private IStoreEvents GetEventStore()
        {
            return Wireup.Init().UsingMongoPersistence(() => settings.RemoteEsConnectionString, new DocumentObjectSerializer()).Build();
        }

        public RebuildContext CreateContext(ILifetimeScope contextScope, string[] buckets, string[] projectionNames, bool revertSelection, bool writeCheckpoint)
        {
            return new RebuildContext(contextScope, waitStrategyFactory, ringSize, rebuilder, logger, buckets, projectionNames, revertSelection, writeCheckpoint);
        }

        public RebuildResult Rebuild(string[] buckets, string[] projectionNames, bool revertSelection)
        {
            using (var rebuildScope = scope.BeginLifetimeScope())
            using (var ctx = CreateContext(rebuildScope, buckets, projectionNames, revertSelection, false))
            {
                try
                {
                    ctx.BeginRebuild();

                    var commits = GetEventStore().Advanced.GetFrom();
                    foreach (var commit in commits)
                    {
                        ctx.Process(commit);
                    }
                }
                finally
                {
                    ctx.EndRebuild();
                }
                return ctx.GetRebuildResult();
            }


        }

    }
}
