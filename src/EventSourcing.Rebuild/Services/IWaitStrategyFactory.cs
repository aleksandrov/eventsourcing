﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Disruptor;

namespace EventSourcing.Rebuild.Services
{
    public interface IWaitStrategyFactory
    {
        IWaitStrategy CreateWaitStrategy();
    }
}
