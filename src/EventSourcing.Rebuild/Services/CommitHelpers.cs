﻿using System.Collections.Generic;
using System.Linq;
using EventSourcing.Cqrs.Messages.Events;
using EventSourcing.EventStore;
using EventSourcing.Interfaces.Services;
using NEventStore;

namespace EventSourcing.Rebuild.Services
{
    static class CommitHelpers
    {
        public static IEnumerable<IEvent> ExtractEventsFromCommit(this ICommit commit)
        {

            if (commit.Headers.Keys.Any(s => s == CommitDispatcher.SAGA_TYPE_HEADER))
            {
                // saga commit. if saga do not replay events from Events, but replay from Undispatched messages
                IList<object> messages = commit.Headers.Where(x => x.Key.StartsWith(CommitDispatcher.UNDISPATCHED_MESSAGES_HEADER_PREFIX)).Select(x => x.Value).ToList();
                foreach (var e in messages.OfType<IEvent>())
                {
                    yield return e;
                }
            }
            else
            {
                // aggregate commit.
                foreach (var e in commit.Events.Select(e => (IEvent)e.Body))
                {
                    yield return e;
                }

            }
        }

        public static void UpdateHeaders(this ICommit commit, IHeaderManager headerManager)
        {
            if (commit.Headers.Keys.Any(s => s == CommitDispatcher.SAGA_TYPE_HEADER))
            {
                IList<object> messages = commit.Headers.Where(x => x.Key.StartsWith(CommitDispatcher.UNDISPATCHED_MESSAGES_HEADER_PREFIX)).Select(x => x.Value).ToList();
                foreach (var @event in messages.OfType<IEvent>())
                {
                    headerManager.UpdateEvent(@event, commit.Headers);
                }
            }
            else
            {
                // get events from aggregates
                foreach (var @event in commit.Events)
                {
                    var e = @event.Body as IEvent;
                    headerManager.UpdateEvent(e, commit.Headers);
                }
            }
        }
    }
}
