﻿namespace EventSourcing.Rebuild.Services
{
    interface IRemoteUpdateSettings
    {
        bool IgnoreUnkownMessages { get; }

        int PullInterval { get; }

        string RemoteEsConnectionString { get; }
    }
}
