﻿namespace EventSourcing.Rebuild.Services.Checkpoints
{
    public interface IStoreCheckpoint
    {
        void SaveCheckpoint(string checkpointToken);
        string LoadCheckpoint();
    }
}
