﻿namespace EventSourcing.Rebuild.Services.Checkpoints
{
    class NullCheckpointStorage : IStoreCheckpoint
    {
        public void SaveCheckpoint(string checkpointToken)
        {
            
        }

        public string LoadCheckpoint()
        {
            return null;
        }
    }
}
