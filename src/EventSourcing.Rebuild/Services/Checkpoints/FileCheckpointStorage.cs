﻿using System;
using System.IO;

namespace EventSourcing.Rebuild.Services.Checkpoints
{
    public class FileCheckpointStorage : IStoreCheckpoint
    {
        private readonly string fullPath;

        public FileCheckpointStorage(string filename)
        {
            fullPath = AppDomain.CurrentDomain.BaseDirectory + filename;
        }

        public void SaveCheckpoint(string checkpointToken)
        {
            File.WriteAllText(fullPath, checkpointToken);
        }

        public string LoadCheckpoint()
        {
            if (!File.Exists(fullPath)) return null;

            return File.ReadAllText(fullPath);
        }
    }
}
