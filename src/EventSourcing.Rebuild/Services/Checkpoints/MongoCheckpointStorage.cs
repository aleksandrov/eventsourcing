﻿using System.Linq;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace EventSourcing.Rebuild.Services.Checkpoints
{
    public class MongoCheckpointStorage : IStoreCheckpoint
    {
        private readonly string checkpointName;

        #region dto

        public class Checkpoint
        {
            public string Id { get; set; }

            public string Value { get; set; }
        }

        #endregion

        private readonly MongoCollection<Checkpoint> checkpoints;

        public MongoCheckpointStorage(MongoDatabase database, string checkpointName, string collectionName)
        {
            this.checkpointName = checkpointName;
            checkpoints = database.GetCollection<Checkpoint>(collectionName);
        }

        public void SaveCheckpoint(string checkpointToken)
        {
            checkpoints.Update(Query<Checkpoint>.EQ(c => c.Id, checkpointName), Update<Checkpoint>.Set(c => c.Value, checkpointToken), UpdateFlags.Upsert);
        }

        public string LoadCheckpoint()
        {
            var checkpoint = checkpoints.AsQueryable().SingleOrDefault(c => c.Id == checkpointName);
            if (checkpoint == null) return null;
            return checkpoint.Value;
        }
    }
}
