﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Autofac;
using Autofac.Core;
using EventSourcing.Cqrs.Autofac;
using EventSourcing.Rebuild.Disruptor;
using EventSourcing.Rebuild.Handlers;
using EventSourcing.Rebuild.Services;
using MongoDB.Bson.Serialization;
using NEventStore;
using NEventStore.Serialization;
using Module = Autofac.Module;

namespace EventSourcing.Rebuild
{
    public class MongoRebuildModule : Module
    {
        private readonly string connectionString;
        private readonly IEnumerable<Assembly> messageAssemblies;
        private readonly int ringSize;
        private readonly int pullInterval;
        private readonly bool remoteUpdatesSupport;
        private readonly bool ignoreUnkownMessages;

        #region Startup

        public class StartupScript : IStartable
        {
            private readonly IEnumerable<Assembly> messageAssemblies;

            public StartupScript(IEnumerable<Assembly> messageAssemblies)
            {
                this.messageAssemblies = messageAssemblies;
            }

            public void Start()
            {
                InitMapping();
            }

            private void InitMapping()
            {
                var types = messageAssemblies.SelectMany(a => a.GetTypes()).ToList();
                foreach (var type in types)
                {
                    if (type.IsInterface) continue;
                    if (type.IsAbstract) continue;

                    if (!BsonClassMap.IsClassMapRegistered(type))
                    {
                        BsonClassMap.RegisterClassMap(new RuntimeClassMap(type));
                    }
                }
            }
        }

        #endregion

        #region Custom ClassMap

        private class RuntimeClassMap : BsonClassMap
        {
            public RuntimeClassMap(Type classType)
                : base(classType)
            {
                AutoMap();
                SetIgnoreExtraElements(true);
            }
        }

        #endregion

        public MongoRebuildModule(string connectionString, IEnumerable<Assembly> messageAssemblies, int ringSize = 1024, int pullInterval = 5000, bool remoteUpdatesSupport = false, bool ignoreUnkownMessages = false)
        {
            this.connectionString = connectionString;
            this.messageAssemblies = messageAssemblies;
            this.ringSize = ringSize;
            this.pullInterval = pullInterval;
            this.remoteUpdatesSupport = remoteUpdatesSupport;
            this.ignoreUnkownMessages = ignoreUnkownMessages;
        }

        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterModule(new DisruptorModule(ringSize));
            builder.RegisterHandler<RebuildProjectionsHandlers>();
            if (remoteUpdatesSupport)
            {
                builder.RegisterHandler<PullRemoteUpdatesHandlers>();
            }

            builder.Register(ctx => new StartupScript(messageAssemblies)).As<IStartable>();
            builder.Register(ctx => new SettingsService(ignoreUnkownMessages, pullInterval, connectionString)).SingleInstance().AsImplementedInterfaces();
            
        }


    }
}
