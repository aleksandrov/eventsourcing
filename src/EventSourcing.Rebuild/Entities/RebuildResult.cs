﻿using System;

namespace EventSourcing.Rebuild.Entities
{
    class RebuildResult
    {
        public TimeSpan Elapsed { get; set; }
    }
}
