﻿using EventSourcing.Cqrs.Messages;
using EventSourcing.Interfaces;
using EventSourcing.Rebuild.Messages.Commands;
using EventSourcing.Rebuild.Services;

namespace EventSourcing.Rebuild.Handlers
{

    class RebuildProjectionsHandlers : IHandles<RebuildProjectionsCommand>
    {
        private readonly IRebuildService service;

        public RebuildProjectionsHandlers(IRebuildService service)
        {
            this.service = service;
        }

        public ExecutionResult Handle(RebuildProjectionsCommand message)
        {
            var result = service.Rebuild(message.Buckets, message.ProjectionNames, message.RevertProjectionsSelection);
            return new ExecutionResult() { Message = string.Format("Rebuild took: {0}", result.Elapsed) };
        }





    }


}
