﻿using EventSourcing.Interfaces;
using EventSourcing.Interfaces.Services;
using EventSourcing.Rebuild.Messages.Events;

namespace EventSourcing.Rebuild.Handlers
{
    class CheckpointHandlers : 
        IConsume<CheckpointChanged>,
        IConsume<CommitFound>
    {
        private readonly ILogger logger;

        public CheckpointHandlers(ILogger logger)
        {
            this.logger = logger;
        }

        public void Consume(CheckpointChanged message)
        {
            int checkpoint = int.Parse(message.Checkpoint);
            if (checkpoint % 1000 == 0)
            {
                logger.InfoFormat("Checkoint changed: ", checkpoint);
            }
        }

        public void Consume(CommitFound message)
        {

        }
    }
}
