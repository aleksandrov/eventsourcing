﻿using System;
using System.Globalization;
using System.IO;
using System.Threading;
using Autofac;
using Autofac.Features.OwnedInstances;
using EventSourcing.Interfaces;
using EventSourcing.Interfaces.Services;
using EventSourcing.Rebuild.Disruptor;
using EventSourcing.Rebuild.Messages.Commands;
using EventSourcing.Rebuild.Messages.Events;
using EventSourcing.Rebuild.Services;
using EventSourcing.Rebuild.Services.Checkpoints;
using MongoDB.Bson;
using NEventStore;
using NEventStore.Client;
using NEventStore.Serialization;

namespace EventSourcing.Rebuild.Handlers
{
    internal class PullRemoteUpdatesHandlers : 
        IProjection,
        IConsume<StartRemoteUpdatesPollingCommand>,
        IConsume<RemoteUpdatesPollingTerminated>,
        IConsume<RemoteUpdatedRequested>
    {
        private readonly IStoreCheckpoint checkpointStorage;
        private readonly IEventPublisher publisher;
        private readonly IRebuildService rebuildService;
        private readonly ILogger logger;
        private readonly IRemoteUpdateSettings settings;
        private readonly ILifetimeScope scope;

        public PullRemoteUpdatesHandlers(ILifetimeScope scope, IEventPublisher publisher, IRebuildService rebuildService, ILogger logger, IStoreCheckpoint checkpointStorage, IRemoteUpdateSettings settings)
        {
            this.checkpointStorage = checkpointStorage;
            this.publisher = publisher;
            this.rebuildService = rebuildService;
            this.logger = logger;
            this.settings = settings;
            this.scope = scope;
        }

        private IStoreEvents GetEventStore()
        {
            return Wireup.Init().UsingMongoPersistence(() => settings.RemoteEsConnectionString, new DocumentObjectSerializer()).Build();
        }

        public void Consume(StartRemoteUpdatesPollingCommand message)
        {
            publisher.Publish(new RemoteUpdatedRequested(message.ProjectionNames, message.Buckets, message.RevertProjectionsSelection));
        }

        public void Consume(RemoteUpdatesPollingTerminated message)
        {
            logger.DebugFormat("Reconnecting...");
            this.ExecuteWrapped(message.ProjectionNames, message.Buckets, message.RevertProjectionsSelection);
        }

        public void Consume(RemoteUpdatedRequested message)
        {
            this.ExecuteWrapped(message.ProjectionNames, message.Buckets, message.RevertProjectionsSelection);
        }

        private void ExecuteWrapped(string[] projectionNames, string[] buckets, bool revertProjectionsSelection)
        {
            try
            {
                Execute(projectionNames, buckets, revertProjectionsSelection);
            }
            catch (Exception ex)
            {
                publisher.Publish(new RemoteUpdatesPollingTerminated(projectionNames, buckets, revertProjectionsSelection, ex.Message, ex.StackTrace));
                logger.DebugFormat("Terminated.");
            }
        }

        private void Execute(string[] projectionNames, string[] buckets, bool revertProjectionsSelection)
        {
            string checkpointToken = checkpointStorage.LoadCheckpoint();
            using (IStoreEvents storeEvents = GetEventStore())
            {
                var pollingClient = new PollingClient(storeEvents.Advanced, settings.PullInterval);
                var observer = new PublishCommitObserver(scope, publisher, rebuildService, buckets, projectionNames, revertProjectionsSelection, checkpointToken, checkpointStorage.SaveCheckpoint, settings);
                using (IObserveCommits observeCommits = pollingClient.ObserveFrom(checkpointToken))
                {
                    using (observeCommits.Subscribe(observer))
                    {
                        observeCommits.Start();
                        if (!string.IsNullOrEmpty(checkpointToken))
                        {
                            logger.InfoFormat("Continue from checkpoint {0}", checkpointToken);
                        }
                        else
                        {
                            observer.Clear();
                        }

                        while (observer.Error == null)
                        {
                            Thread.Sleep(100);
                        }
                        logger.Error(string.Format("Error occured: {0}", observer.Error.Message), observer.Error);
                        throw observer.Error;
                    }
                }
            }
        }

        private class PublishCommitObserver : IObserver<ICommit>
        {
            private readonly IEventPublisher publisher;
            private readonly Action<string> saveCheckpointAction;
            private readonly IRemoteUpdateSettings remoteUpdateSettings;

            private string lastKnownCheckpoint;
            private RebuildContext context;

            public Exception Error { get; private set; }

            public PublishCommitObserver(ILifetimeScope scope, IEventPublisher publisher, IRebuildService rebuildService, string[] buckets, string[] projectionNames, bool revertProjectionsSelection, string checkpointToken, Action<string> saveCheckpointAction, IRemoteUpdateSettings remoteUpdateSettings)
            {
                this.publisher = publisher;
                this.saveCheckpointAction = saveCheckpointAction;
                this.remoteUpdateSettings = remoteUpdateSettings;

                lastKnownCheckpoint = checkpointToken;

                context = rebuildService.CreateContext(scope, buckets, projectionNames, revertProjectionsSelection, writeCheckpoint: true);
            }

            public void Clear()
            {
                context.BeginRebuild();
            }

            public void OnNext(ICommit commit)
            {
                context.Process(commit);

                // TODO: ensure that commit is processed by all projections
                //saveCheckpointAction(commit.CheckpointToken);
                lastKnownCheckpoint = commit.CheckpointToken;
                //publisher.Publish(new CheckpointChanged(commit.CheckpointToken) { Created = DateTime.UtcNow });
            }

            public void OnError(Exception error)
            {
                if (error is FileFormatException)
                {
                    if (remoteUpdateSettings.IgnoreUnkownMessages)
                    {
                        SkipCommit();
                    }
                }

                if (error is BsonSerializationException)
                {
                    if (remoteUpdateSettings.IgnoreUnkownMessages)
                    {
                        SkipCommit();
                    }
                }

                Error = error;    
            }

            private void SkipCommit()
            {
                lastKnownCheckpoint = (int.Parse(lastKnownCheckpoint) + 1).ToString(CultureInfo.InvariantCulture);
                saveCheckpointAction(lastKnownCheckpoint);
                publisher.Publish(new CheckpointChanged(lastKnownCheckpoint) { Created = DateTime.UtcNow });
            }

            public void OnCompleted()
            {
            }
        }

        public void OnRebuildStarted()
        {
            
        }

        public void OnRebuildFinished()
        {
            
        }
    }

}
