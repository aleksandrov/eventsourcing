﻿using System.Collections.Generic;
using System.Linq;
using Disruptor;
using EventSourcing.Cqrs.Messages.Events;
using EventSourcing.EventStore;
using EventSourcing.Interfaces.Services;
using EventSourcing.Rebuild.Disruptor.Messages;
using NEventStore;

namespace EventSourcing.Rebuild.Disruptor.Handlers
{
    class ParseHeadersHandler : IEventHandler<CommitMessage>
    {
        private readonly IHeaderManager headerManager;
        private readonly ILogger logger;

        public ParseHeadersHandler(IHeaderManager headerManager, ILogger logger)
        {
            this.headerManager = headerManager;
            this.logger = logger;
        }

        public void OnNext(CommitMessage data, long sequence, bool endOfBatch)
        {
            UpdateHeaders(data.Commit);
            data.Events = ExtractEventsFromCommit(data.Commit).ToArray();
        }

        private IEnumerable<IEvent> ExtractEventsFromCommit(ICommit commit)
        {

            if (commit.Headers != null && commit.Headers.Keys.Any(s => s == CommitDispatcher.SAGA_TYPE_HEADER))
            {
                // saga commit. if saga do not replay events from Events, but replay from Undispatched messages
                IList<object> messages = commit.Headers.Where(x => x.Key.StartsWith(CommitDispatcher.UNDISPATCHED_MESSAGES_HEADER_PREFIX)).Select(x => x.Value).ToList();
                foreach (var e in messages.OfType<IEvent>())
                {
                    yield return e;
                }
            }
            else
            {
                // aggregate commit.
                foreach (var e in commit.Events.Select(e => (IEvent)e.Body))
                {
                    yield return e;
                }

            }
        }

        private void UpdateHeaders(ICommit commit)
        {
            if (commit.Headers != null && commit.Headers.Keys.Any(s => s == CommitDispatcher.SAGA_TYPE_HEADER))
            {
                IList<object> messages = commit.Headers.Where(x => x.Key.StartsWith(CommitDispatcher.UNDISPATCHED_MESSAGES_HEADER_PREFIX)).Select(x => x.Value).ToList();
                foreach (var @event in messages.OfType<IEvent>())
                {
                    headerManager.UpdateEvent(@event, commit.Headers);
                }
            }
            else
            {
                // get events from aggregates
                foreach (var @event in commit.Events)
                {
                    var e = @event.Body as IEvent;
                    headerManager.UpdateEvent(e, commit.Headers);
                }
            }
        }
    }
}
