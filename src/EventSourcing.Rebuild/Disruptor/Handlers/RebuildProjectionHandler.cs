﻿using Disruptor;
using EventSourcing.Interfaces.History;
using EventSourcing.Rebuild.Disruptor.Messages;

namespace EventSourcing.Rebuild.Disruptor.Handlers
{
    class RebuildProjectionHandler : IEventHandler<CommitMessage>
    {
        private readonly IRebuildable endpoint;

        public RebuildProjectionHandler(IRebuildable endpoint)
        {
            this.endpoint = endpoint;
        }

        public void OnNext(CommitMessage data, long sequence, bool endOfBatch)
        {
            foreach (var e in data.Events)
            {
                endpoint.Rebuild(e, data.Scope);
            }
        }
    }
}
