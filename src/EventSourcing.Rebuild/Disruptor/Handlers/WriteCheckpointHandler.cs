﻿using Disruptor;
using EventSourcing.Interfaces.Services;
using EventSourcing.Rebuild.Disruptor.Messages;
using EventSourcing.Rebuild.Services.Checkpoints;

namespace EventSourcing.Rebuild.Disruptor.Handlers
{
    class WriteCheckpointHandler : IEventHandler<CommitMessage>
    {
        private readonly IEventPublisher publisher;
        private readonly IStoreCheckpoint checkpointStorage;

        public WriteCheckpointHandler(IStoreCheckpoint checkpointStorage)
        {
            this.publisher = publisher;
            this.checkpointStorage = checkpointStorage;
        }

        public void OnNext(CommitMessage data, long sequence, bool endOfBatch)
        {
            checkpointStorage.SaveCheckpoint(data.Commit.CheckpointToken);
            //publisher.Publish(new CheckpointChanged(data.Commit.CheckpointToken));
        }
    }
}
