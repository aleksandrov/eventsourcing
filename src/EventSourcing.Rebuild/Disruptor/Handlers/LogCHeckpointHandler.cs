﻿using Disruptor;
using EventSourcing.Interfaces.Services;
using EventSourcing.Rebuild.Disruptor.Messages;
using EventSourcing.Rebuild.Messages.Events;

namespace EventSourcing.Rebuild.Disruptor.Handlers
{
    class LogCheckpointHandler : IEventHandler<CommitMessage>
    {
        private readonly ILogger logger;
        private readonly IEventPublisher publisher;

        public LogCheckpointHandler(ILogger logger, IEventPublisher publisher)
        {
            this.logger = logger;
            this.publisher = publisher;
        }

        public void OnNext(CommitMessage data, long sequence, bool endOfBatch)
        {
            publisher.Publish(new CheckpointChanged(data.Commit.CheckpointToken));
            if (sequence%1000 == 0)
            {
                logger.InfoFormat("Checkpoint found: {0}", data.Commit.CheckpointToken);
            }
        }
    }
}
