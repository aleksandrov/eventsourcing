﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Disruptor;
using EventSourcing.Rebuild.Services;

namespace EventSourcing.Rebuild.Disruptor
{
    class DefaultWaitStrategyFactory : IWaitStrategyFactory
    {
        public IWaitStrategy CreateWaitStrategy()
        {
            return new BlockingWaitStrategy();
        }
    }
}
