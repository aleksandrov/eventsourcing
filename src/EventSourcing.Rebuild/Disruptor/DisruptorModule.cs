﻿using Autofac;
using Autofac.Core;
using EventSourcing.Rebuild.Disruptor.Handlers;
using EventSourcing.Rebuild.Services;
using EventSourcing.Rebuild.Services.Checkpoints;
using NEventStore;

namespace EventSourcing.Rebuild.Disruptor
{
    class DisruptorModule : Module
    {
        private readonly int ringSize;

        public DisruptorModule(int ringSize = 1024)
        {
            this.ringSize = ringSize;
        }

        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<NullCheckpointStorage>().AsImplementedInterfaces().OwnedByLifetimeScope();

            builder.RegisterType<ParseHeadersHandler>().AsSelf();
            builder.RegisterType<WriteCheckpointHandler>().AsSelf();
            builder.RegisterType<LogCheckpointHandler>().AsSelf();
            builder.RegisterType<DefaultWaitStrategyFactory>().AsImplementedInterfaces();

            builder.RegisterType<RebuildService>().AsImplementedInterfaces()
                .WithParameter(new NamedParameter("ringSize", ringSize))
                .WithParameter(ResolvedParameter.ForNamed<IStoreEvents>(Consts.EventStoreName));
        }
    }
}
