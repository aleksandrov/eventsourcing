﻿using Autofac;
using EventSourcing.Cqrs.Messages.Events;
using NEventStore;

namespace EventSourcing.Rebuild.Disruptor.Messages
{
    class CommitMessage
    {
        public ICommit Commit { get; set; }

        public ILifetimeScope Scope { get; set; }

        public IEvent[] Events { get; set; }
    }
}
