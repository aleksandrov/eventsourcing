﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Disruptor;
using Disruptor.Dsl;
using EventSourcing.Interfaces.History;
using EventSourcing.Interfaces.Services;
using EventSourcing.Rebuild.Disruptor.Handlers;
using EventSourcing.Rebuild.Disruptor.Messages;
using EventSourcing.Rebuild.Entities;
using EventSourcing.Rebuild.Messages.Events;
using EventSourcing.Rebuild.Services;
using NEventStore;

namespace EventSourcing.Rebuild.Disruptor
{
    class RebuildContext : IDisposable
    {
        private readonly ILifetimeScope scope;
        private readonly IWaitStrategyFactory waitStrategyFactory;
        private readonly int ringSize;
        private readonly IRebuildsProjections rebuilder;
        private readonly ILogger logger;
        private readonly string[] buckets;
        private readonly string[] projectionNames;
        private readonly bool revertSelection;
        private readonly bool writeCheckpoint;
        private Disruptor<CommitMessage> rebuildDisruptor;
        private Stopwatch watch;

        private bool isStarted;
        private bool isFinished;
        private bool isDisposed;

        #region fake commit

        public class InternalCommit : ICommit
        {
            public string BucketId { get; set; }
            public string StreamId { get; set; }
            public int StreamRevision { get; set; }
            public Guid CommitId { get; set; }
            public int CommitSequence { get; set; }
            public DateTime CommitStamp { get; set; }
            public IDictionary<string, object> Headers { get; set; }
            public ICollection<EventMessage> Events { get; set; }
            public string CheckpointToken { get; set; }
        }

        #endregion


        public RebuildContext(ILifetimeScope scope, IWaitStrategyFactory waitStrategyFactory, int ringSize, IRebuildsProjections rebuilder, ILogger logger, string[] buckets, string[] projectionNames, bool revertSelection, bool writeCheckpoint)
        {
            this.scope = scope;
            this.waitStrategyFactory = waitStrategyFactory;
            this.ringSize = ringSize;
            this.rebuilder = rebuilder;
            this.logger = logger;
            this.buckets = buckets;
            this.projectionNames = projectionNames;
            this.revertSelection = revertSelection;
            this.writeCheckpoint = writeCheckpoint;

            watch = new Stopwatch();
        }

        private Disruptor<CommitMessage> GetDisruptor()
        {
            if (rebuildDisruptor == null)
            {
                rebuildDisruptor = new Disruptor<CommitMessage>(() => new CommitMessage(), new MultiThreadedLowContentionClaimStrategy(ringSize), waitStrategyFactory.CreateWaitStrategy(), TaskScheduler.Default);
                var preProcessGroup = rebuildDisruptor.HandleEventsWith(scope.Resolve<ParseHeadersHandler>(), scope.Resolve<LogCheckpointHandler>());
                var projectionsGroup = preProcessGroup.Then(GetProjectionHandlers());

                if (writeCheckpoint)
                {
                    projectionsGroup.Then(scope.Resolve<WriteCheckpointHandler>());
                }

                rebuildDisruptor.Start();
            }

            return rebuildDisruptor;
        }

        private IEventHandler<CommitMessage>[] GetProjectionHandlers()
        {
            return rebuilder.GetRebuildableProjections(projectionNames, revertSelection)
                .Select(r => new RebuildProjectionHandler(r))
                .Cast<IEventHandler<CommitMessage>>()
                .ToArray();
        }

        public void BeginRebuild()
        {
            AssertDisposed();
            isStarted = true;
            watch.Start();

            if (projectionNames == null)
            {
                logger.InfoFormat("Cleaning up all projections.");
            }
            else
            {
                logger.InfoFormat("Cleaning up projections: {0}", string.Join(", ", projectionNames));
            }

            PublishCommit(CreateRebuildStartedCommit());

            logger.InfoFormat("Projections are cleaned up.");
        }

        public void EndRebuild()
        {
            AssertDisposed();

            logger.InfoFormat("All events are replayed. Performing post-rebuild tasks.");
            PublishCommit(CreateRebuildFinishedCommit());

            watch.Stop();
            isFinished = true;
        }

        public void Process(ICommit commit)
        {
            AssertDisposed();

            if (buckets != null && buckets.Length > 0 && !buckets.Contains(commit.BucketId)) return;

            PublishCommit(commit);
        }

        public void Dispose()
        {
            AssertDisposed();

            isDisposed = true;
            if (rebuildDisruptor == null) return; 
            rebuildDisruptor.Shutdown();
        }

        public RebuildResult GetRebuildResult()
        {
            if (!isStarted || !isFinished)
            {
                throw new ApplicationException("No rebuild result was generated.");
            }

            return new RebuildResult
            {
                Elapsed = watch.Elapsed
            };
        }

        private void PublishCommit(ICommit commit)
        {
            GetDisruptor().PublishEvent((msg, sequence) =>
            {
                msg.Commit = commit;
                msg.Scope = scope;
                return msg;
            });
        }

        private void AssertDisposed()
        {
            if (isDisposed) throw new InvalidOperationException("Context is already disposed.");
        }

        private ICommit CreateRebuildStartedCommit()
        {
            var result = new InternalCommit();
            result.Headers = new Dictionary<string, object>();
            result.Events = new List<EventMessage>();
            result.Events.Add(new EventMessage() { Body = new RebuildStarted() });
            return result;
        }

        private ICommit CreateRebuildFinishedCommit()
        {
            var result = new InternalCommit();
            result.Headers = new Dictionary<string, object>();
            result.Events = new List<EventMessage>();
            result.Events.Add(new EventMessage() { Body = new RebuildFinished() });
            return result;
        }
    }
}
