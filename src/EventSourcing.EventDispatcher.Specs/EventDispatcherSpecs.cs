﻿using System;
using System.Collections.Generic;
using Autofac;
using EventSourcing.Cqrs.Messages;
using EventSourcing.Cqrs.Messages.Commands;
using EventSourcing.EventDispatcher.Endpoints;
using EventSourcing.EventDispatcher.Routing;
using EventSourcing.Interfaces;
using FluentAssertions;
using Machine.Fakes;
using Machine.Specifications;
using NSubstitute;

namespace EventSourcing.EventDispatcher.Specs
{
    [Subject(typeof(EventDispatcher))]
    public class when_send_new_command_with_id_set : EventDispatcherSpecs
    {
        Establish context = () =>
        {
            id = Guid.NewGuid();
            command = new TestCommand() { Id = id};
            commandScope = An<ILifetimeScope>();
            The<IRoutingTable>().GetEndpoints(Arg.Any<IMessage>()).Returns(new List<IEndpoint> { The<IEndpoint>() });
            The<ILifetimeScope>().BeginLifetimeScope().Returns(commandScope);
        };

        private Because of = () => Subject.Send(command);

        private It should_create_new_lifetime_scope_for_the_command = () => The<ILifetimeScope>().Received().BeginLifetimeScope();

        private It should_send_command_to_one_endpoint = () => The<IEndpoint>().Received().Handle(command, commandScope);

        private It should_keep_command_id_same = () => command.Id.Should().Be(id);

        private static TestCommand command;
        private static Guid id;
        private static ILifetimeScope commandScope;
    }

    [Subject(typeof(EventDispatcher))]
    class when_executing_command_with_no_id_set : EventDispatcherSpecs
    {
        Establish context = () =>
        {
            command = new TestCommand();
            commandScope = An<ILifetimeScope>();

            The<IRoutingTable>().GetEndpoints(Arg.Any<IMessage>()).Returns(new List<IEndpoint> { The<IEndpoint>() });
            The<ILifetimeScope>().BeginLifetimeScope().Returns(commandScope);
        };

        private Because of = () => Subject.Send(command);

        private It should_create_new_lifetime_scope_for_the_command = () => The<ILifetimeScope>().Received().BeginLifetimeScope();

        private It should_send_command_to_one_endpoint = () => The<IEndpoint>().Received().Handle(command, commandScope);

        private It should_generate_new_command_id = () => command.Id.Should().NotBeEmpty();

        private static TestCommand command;
        private static ILifetimeScope commandScope;
    }

    [Subject(typeof(EventDispatcher))]
    public class when_more_than_one_endpoints_found_for_the_command : EventDispatcherSpecs
    {
        Establish context = () =>
        {
            endpoint1 = Substitute.For<IEndpoint>();
            endpoint2 = Substitute.For<IEndpoint>();
            The<IRoutingTable>().GetEndpoints(Arg.Any<IMessage>()).Returns(new List<IEndpoint> { endpoint1, endpoint2 });            
        };

        private Because of = () => action = () => Subject.Send(new TestCommand());

        private It should_throw_error = () => action.ShouldThrow<InvalidOperationException>();

        private static IEndpoint endpoint1;
        private static IEndpoint endpoint2;
        private static Action action;
    }



    public class EventDispatcherSpecs : WithSubject<EventDispatcher>
    {
        #region stubs

        public class TestCommand : Command
        {
            public DateTime Created { get; set; }
        }

        public class CommandHandlerOne : IHandles<TestCommand>
        {
            public ExecutionResult Handle(TestCommand message)
            {
                throw new NotImplementedException();
            }
        }

        public class CommandHandlerTwo : IHandles<TestCommand>
        {
            public ExecutionResult Handle(TestCommand message)
            {
                throw new NotImplementedException();
            }
        }

        #endregion
    }


}
