﻿using System;
using EventSourcing.Cqrs.Messages;
using EventSourcing.Cqrs.Messages.Commands;
using EventSourcing.Cqrs.Messages.Events;
using EventSourcing.EventDispatcher.Endpoints;
using EventSourcing.Interfaces;
using FluentAssertions;
using Machine.Specifications;

namespace EventSourcing.EventDispatcher.Specs.Endpoints
{

    [Subject(typeof(ActionCache<>))]
    public class when_generated_call_of_IConsume_of_IEvent_executed
    {
        #region stubs

        public class TestEvent : IEvent
        {
            public DateTime Created { get; set; }
            public int Version { get; set; }
            public Guid CorrelationId { get; set; }
        }

        public class TestHandler : IProjection,
                                   IConsume<TestEvent>
        {
            public bool Fired;

            public void Consume(TestEvent message)
            {
                Fired = true;
            }

            public string Name { get; private set; }

            public void OnRebuildStarted()
            {
                throw new NotImplementedException();
            }

            public void OnRebuildFinished()
            {
                throw new NotImplementedException();
            }
        }

        #endregion 

        private static TestHandler handler;
        

        Establish context = () =>
        {
            handler = new TestHandler();
            action = ActionCache<TestHandler>.BuildCall(typeof(TestEvent));
        };

        private Because of = () => action(handler, new TestEvent());

        private It should_execute_handler = () => handler.Fired.Should().BeTrue();
        private static Func<TestHandler, IMessage, ExecutionResult> action;
    }

    [Subject(typeof(ActionCache<>))]
    public class when_generated_call_of_IConsume_of_ICommand_executed
    {
        #region stubs

        public class TestCommand : Command
        {
            public DateTime Created { get; set; }
            public int Version { get; set; }
        }

        public class TestHandler : IConsume<TestCommand>
        {
            public bool Fired;

            public void Consume(TestCommand message)
            {
                Fired = true;
            }
        }

        #endregion 

        private static TestHandler handler;
        

        Establish context = () =>
        {
            handler = new TestHandler();
            action = ActionCache<TestHandler>.BuildCall(typeof(TestCommand));
        };

        private Because of = () => action(handler, new TestCommand());

        private It should_execute_handler = () => handler.Fired.Should().BeTrue();

        private static Func<TestHandler, IMessage, ExecutionResult> action;
    }

    [Subject(typeof(ActionCache<>))]
    public class when_generated_call_of_IHandle_of_ICommand_executed
    {
        #region stubs

        public class TestCommand : Command
        {
            public DateTime Created { get; set; }
            public int Version { get; set; }
        }

        public class TestHandler : IHandles<TestCommand>
        {
            public bool Fired;

            public ExecutionResult Handle(TestCommand message)
            {
                Fired = true;
                return ExecutionResult.Empty;
            }
        }

        #endregion

        private static TestHandler handler;

        Establish context = () =>
        {
            handler = new TestHandler();
            action = ActionCache<TestHandler>.BuildCall(typeof(TestCommand));
        };

        private Because of = () => action(handler, new TestCommand());

        private It should_execute_handler = () => handler.Fired.Should().BeTrue();
        private static Func<TestHandler, IMessage, ExecutionResult> action;
    }

    [Subject(typeof(ActionCache<>))]
    public class when_generated_call_of_IHandle_of_IEvent_executed
    {
        #region stubs

        public class TestEvent : IEvent
        {
            public DateTime Created { get; set; }
            public int Version { get; set; }
            public Guid CorrelationId { get; set; }
        }

        public class TestHandler : IHandles<TestEvent>
        {
            public bool Fired;

            public ExecutionResult Handle(TestEvent message)
            {
                Fired = true;
                return ExecutionResult.Empty;
            }
        }

        #endregion

        private static TestHandler handler;

        Establish context = () =>
        {
            handler = new TestHandler();
            action = ActionCache<TestHandler>.BuildCall(typeof(TestEvent));
        };

        private Because of = () => action(handler, new TestEvent());

        private It should_execute_handler = () => handler.Fired.Should().BeTrue();
        private static Func<TestHandler, IMessage, ExecutionResult> action;
    }



}
