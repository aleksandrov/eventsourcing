﻿using Autofac;
using EventSourcing.Cqrs.Messages.Commands;
using EventSourcing.EventDispatcher.Endpoints;
using EventSourcing.EventDispatcher.Endpoints.Async;
using EventSourcing.EventDispatcher.Subscribers.Subscriptions;
using EventSourcing.Interfaces;
using EventSourcing.Interfaces.Services;
using FluentAssertions;
using Machine.Fakes;
using Machine.Specifications;

namespace EventSourcing.EventDispatcher.Specs.Endpoints
{
    public class AutofacEndpointFactorySpec : WithSubject<AutofacEndpointFactory>
    {
        #region subscriber stub

        public class TestCommand : Command
        {
        }

        public class TestSubscriber : IConsume<TestCommand>
        {
            public void Consume(TestCommand message)
            {
                
            }
        }

        #endregion

        Establish context = () =>
        {
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterType<TestSubscriber>().AsSelf().AsImplementedInterfaces();
            builder.RegisterModule(new EventDispatcherModule());
            builder.Register(ctx => The<ILogger>()).As<ILogger>();
            var container = builder.Build();

            Subject = new AutofacEndpointFactory(container);

            subscription = new CommandSubscription(typeof (TestSubscriber), typeof (TestCommand), isAsync: true);
        };

        private Because of = () => endpoint = Subject.CreateCommandHandlerEndpoint(subscription);

        private It should_return_command_endpoint = () => endpoint.Should().BeOfType<AsyncConsumerEndpoint<TestSubscriber>>();

        private static CommandSubscription subscription;
        private static IEndpoint endpoint;
    }
}
