﻿using System;
using System.Threading;
using Autofac;
using EventSourcing.Cqrs.Messages;
using EventSourcing.Cqrs.Messages.Commands;
using EventSourcing.Cqrs.Messages.Events;
using EventSourcing.EventDispatcher.Endpoints;
using EventSourcing.EventDispatcher.Endpoints.Async;
using EventSourcing.Interfaces;
using EventSourcing.Interfaces.Services;
using FluentAssertions;
using Machine.Fakes;
using Machine.Specifications;

namespace EventSourcing.EventDispatcher.Specs
{

    [Subject(typeof(EventDispatcher))]
    public class when_user_sends_command_to_test_command_handler : WithSubject<EventDispatcher>
    {
        #region stub handler

        public class TestCommand : Command
        {

        }

        public class TestCommandHandler : IHandles<TestCommand>
        {
            public ExecutionResult Handle(TestCommand message)
            {
                isHandled = true;
                return ExecutionResult.Empty;
            }
        }

        #endregion

        Establish context = () =>
        {
            command = new TestCommand();

            var builder = new ContainerBuilder();
            builder.RegisterModule(new EventDispatcherModule());
            builder.RegisterType<TestCommandHandler>().AsSelf().AsImplementedInterfaces();
            
            builder.Register(ctx => The<ILogger>()).As<ILogger>();

            var container = builder.Build();

            Subject = container.Resolve<EventDispatcher>();
        };

        Because of = () => Subject.Send(command);

        It should_command_handler_should_handle_that_command = () => isHandled.Should().BeTrue();

        protected static bool isHandled;
        static TestCommand command;
    }

    [Subject(typeof(EventDispatcher))]
    public class when_user_sends_command_to_test_command_handler_and_decorator_defined : WithSubject<EventDispatcher>
    {
        #region stub handler

        public class TestCommand : Command
        {

        }

        public class TestCommandHandler : IHandles<TestCommand>
        {
            public ExecutionResult Handle(TestCommand message)
            {
                isHandled = true;
                return ExecutionResult.Empty;
            }
        }

        public class CommandEndpointDecorator<THandler> : CommandHandlerEndpoint<THandler>
        {
            private readonly CommandHandlerEndpoint<THandler> inner;

            public CommandEndpointDecorator(CommandHandlerEndpoint<THandler> inner, ILogger logger) : base(logger)
            {
                this.inner = inner;
            }

            public override ExecutionResult Handle(IMessage e, ILifetimeScope scope)
            {
                isHandledByDecorator = true;
                return inner.Handle(e, scope);
            }

        }

        #endregion

        Establish context = () =>
        {
            command = new TestCommand();

            var builder = new ContainerBuilder();
            builder.RegisterModule(new EventDispatcherModule());
            builder.RegisterType<TestCommandHandler>().AsSelf().AsImplementedInterfaces();

            builder.Register(ctx => The<ILogger>()).As<ILogger>();

            builder.RegisterGenericDecorator(typeof(CommandEndpointDecorator<>), typeof(CommandHandlerEndpoint<>), fromKey: EventDispatcherModule.COMMAND_ENDPOINT_KEY);

            var container = builder.Build();

            Subject = container.Resolve<EventDispatcher>();
        };

        Because of = () => Subject.Send(command);

        It should_command_handler_should_handle_that_command = () => (isHandled && isHandledByDecorator).Should().BeTrue();

        protected static bool isHandled;
        protected static bool isHandledByDecorator;

        static TestCommand command;
    }

    #region saga endpoints

    [Subject(typeof(EventDispatcher))]
    public class when_user_sends_event_to_saga_command_handler_and_decorator_defined : WithSubject<EventDispatcher>
    {
        #region stub handler

        public class TestEvent : Event
        {

        }

        public class TestSagaHandler : IHandles<TestEvent>
        {
            public ExecutionResult Handle(TestEvent message)
            {
                isHandled = true;
                return ExecutionResult.Empty;
            }
        }

        public class SagaEndpointDecorator<THandler> : SagaEndpointBase<THandler>
        {
            private readonly SagaEndpointBase<THandler> inner;

            public SagaEndpointDecorator(ILifetimeScope endpointScope, ILogger logger, SagaEndpointBase<THandler> inner) : base(endpointScope, logger)
            {
                this.inner = inner;
            }

            public override ExecutionResult Handle(IMessage e, ILifetimeScope scope)
            {
                isHandledByDecorator = true;
                return inner.Handle(e, scope);
            }

        }

        #endregion

        Establish context = () =>
        {
            _event = new TestEvent();

            var builder = new ContainerBuilder();
            builder.RegisterModule(new EventDispatcherModule());
            builder.RegisterType<TestSagaHandler>().AsSelf().AsImplementedInterfaces();

            builder.Register(ctx => The<ILogger>()).As<ILogger>();

            builder.RegisterGenericDecorator(typeof(SagaEndpointDecorator<>), typeof(SagaEndpointBase<>), fromKey: EventDispatcherModule.COMMAND_ENDPOINT_KEY);

            var container = builder.Build();

            Subject = container.Resolve<EventDispatcher>();
        };

        Because of = () => Subject.Publish(_event);

        It should_command_handler_should_handle_that_event = () =>
        {
            Thread.Sleep(500);
            isHandledByDecorator.Should().BeTrue();
            isHandled.Should().BeTrue();
        };

        protected static bool isHandled;
        protected static bool isHandledByDecorator;

        static TestEvent _event;
    }

    [Subject(typeof(EventDispatcher))]
    public class when_user_sends_event_to_saga_command_handler_and_no_decorators_defined : WithSubject<EventDispatcher>
    {
        #region stub handler

        public class TestEvent : Event
        {

        }

        public class TestSagaHandler : IHandles<TestEvent>
        {
            public ExecutionResult Handle(TestEvent message)
            {
                isHandled = true;
                return ExecutionResult.Empty;
            }
        }

        #endregion

        Establish context = () =>
        {
            _event = new TestEvent();

            var builder = new ContainerBuilder();
            builder.RegisterModule(new EventDispatcherModule());
            builder.RegisterType<TestSagaHandler>().AsSelf().AsImplementedInterfaces();

            builder.Register(ctx => The<ILogger>()).As<ILogger>();

            var container = builder.Build();

            Subject = container.Resolve<EventDispatcher>();
        };

        Because of = () => Subject.Publish(_event);

        It should_command_handler_should_handle_that_event = () =>
        {
            Thread.Sleep(500);
            isHandled.Should().BeTrue();
        };

        protected static bool isHandled;
        protected static bool isHandledByDecorator;

        static TestEvent _event;
    }

    #endregion

    [Subject(typeof(EventDispatcher))]
    public class when_user_sends_event_to_projection_handler_and_decorator_defined : WithSubject<EventDispatcher>
    {
        #region stub handler

        public class TestEvent : Event
        {

        }

        public class TestProjection : IProjection, IConsume<TestEvent>
        {
            public void Consume(TestEvent message)
            {
                isHandled = true;
            }

            public void OnRebuildStarted()
            {
                
            }

            public void OnRebuildFinished()
            {
                
            }
        }

        public class ProjectionEndpointDecorator<THandler> : AsyncConsumerEndpoint<THandler>
        {
            private readonly AsyncConsumerEndpoint<THandler> inner;

            public ProjectionEndpointDecorator(ILifetimeScope endpointScope, ILogger logger, AsyncConsumerEndpoint<THandler> inner)
                : base(endpointScope, logger)
            {
                this.inner = inner;
            }

            public override ExecutionResult Handle(IMessage e, ILifetimeScope scope)
            {
                isHandledByDecorator = true;
                return inner.Handle(e, scope);
            }

        }

        #endregion

        Establish context = () =>
        {
            _event = new TestEvent();

            var builder = new ContainerBuilder();
            builder.RegisterModule(new EventDispatcherModule());
            builder.RegisterType<TestProjection>().AsSelf().AsImplementedInterfaces();

            builder.Register(ctx => The<ILogger>()).As<ILogger>();

            builder.RegisterGenericDecorator(typeof(ProjectionEndpointDecorator<>), typeof(AsyncConsumerEndpoint<>), fromKey: EventDispatcherModule.COMMAND_ENDPOINT_KEY);

            var container = builder.Build();

            Subject = container.Resolve<EventDispatcher>();
        };

        Because of = () => Subject.Publish(_event);

        It should_command_handler_should_handle_that_event = () =>
        {
            Thread.Sleep(500);
            (isHandled && isHandledByDecorator).Should().BeTrue();
        };

        protected static bool isHandled;
        protected static bool isHandledByDecorator;

        static TestEvent _event;
    }


    [Subject(typeof(EventDispatcher))]
    public class when_user_sends_event_to_projection_handler_and_no_decorator_defined : WithSubject<EventDispatcher>
    {
        #region stub handler

        public class TestEvent : Event
        {

        }

        public class TestSagaHandler : IConsume<TestEvent>
        {
            public void Consume(TestEvent message)
            {
                isHandled = true;
            }
        }

        #endregion

        Establish context = () =>
        {
            _event = new TestEvent();

            var builder = new ContainerBuilder();
            builder.RegisterModule(new EventDispatcherModule());
            builder.RegisterType<TestSagaHandler>().AsSelf().AsImplementedInterfaces();

            builder.Register(ctx => The<ILogger>()).As<ILogger>();
            var container = builder.Build();

            Subject = container.Resolve<EventDispatcher>();
        };

        Because of = () => Subject.Publish(_event);

        It should_command_handler_should_handle_that_event = () =>
        {
            Thread.Sleep(500);
            isHandled.Should().BeTrue();
        };

        protected static bool isHandled;

        static TestEvent _event;
    }
}
