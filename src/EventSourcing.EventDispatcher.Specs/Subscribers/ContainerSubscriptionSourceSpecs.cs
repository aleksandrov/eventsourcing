﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using EventSourcing.Cqrs.Messages;
using EventSourcing.Cqrs.Messages.Commands;
using EventSourcing.Cqrs.Messages.Events;
using EventSourcing.EventDispatcher.Subscribers;
using EventSourcing.EventDispatcher.Subscribers.Sources;
using EventSourcing.EventDispatcher.Subscribers.Subscriptions;
using EventSourcing.Interfaces;
using FluentAssertions;
using Machine.Fakes;
using Machine.Specifications;

namespace EventSourcing.EventDispatcher.Specs.Subscribers
{

    [Subject(typeof(ContainerSubscriptionSource))]
    public class when_found_IHandle_of_Command_subscription : WithSubject<ContainerSubscriptionSource>
    {
        #region subscriber stub

        public class TestCommand : Command
        {
            public DateTime Created { get; set; }
            public int Version { get; set; }
        }

        public class TestSubscriber : IHandles<TestCommand>
        {
            public ExecutionResult Handle(TestCommand message)
            {
                return ExecutionResult.Empty;
            }
        }

        #endregion

        Establish context = () =>
        {
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterType<TestSubscriber>().AsImplementedInterfaces();

            var container = builder.Build();

            Subject = new ContainerSubscriptionSource(container);
        };

        private Because of = () => subscriptions = Subject.GetSubscriptions();

        private It should_return_one_subscription = () => subscriptions.Should().HaveCount(1);

        private It should_return_async_subscription = () => subscriptions.Single().IsAsync.Should().BeFalse();

        private It should_return_event_subscription = () => subscriptions.Single().Should().BeOfType<CommandSubscription>();

        private static IEnumerable<ISubscription> subscriptions;
    }

    [Subject(typeof(ContainerSubscriptionSource))]
    public class when_found_IHandle_of_Event_subscription : WithSubject<ContainerSubscriptionSource>
    {
        #region subscriber stub

        public class TestEvent : IEvent
        {
            public DateTime Created { get; set; }
            public int Version { get; set; }
            public Guid CorrelationId { get; set; }
        }

        public class TestSubscriber : IHandles<TestEvent>
        {
            public ExecutionResult Handle(TestEvent message)
            {
                return ExecutionResult.Empty;
            }
        }

        #endregion

        Establish context = () =>
        {
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterType<TestSubscriber>().AsImplementedInterfaces();

            var container = builder.Build();

            Subject = new ContainerSubscriptionSource(container);
        };

        private Because of = () => subscriptions = Subject.GetSubscriptions();

        private It should_return_one_subscription = () => subscriptions.Should().HaveCount(1);

        private It should_return_async_subscription = () => subscriptions.Single().IsAsync.Should().BeFalse();

        private It should_return_event_subscription = () => subscriptions.Single().Should().BeOfType<SagaSubscription>();

        private static IEnumerable<ISubscription> subscriptions;
    }

    [Subject(typeof(ContainerSubscriptionSource))]
    public class when_found_IConsume_of_Event_subscription : WithSubject<ContainerSubscriptionSource>
    {
        #region subscriber stub

        public class TestEvent : IEvent
        {
            public DateTime Created { get; set; }
            public int Version { get; set; }
            public Guid CorrelationId { get; set; }
        }

        public class TestSubscriber : IConsume<TestEvent>
        {
            public void Consume(TestEvent message)
            {

            }
        }

        #endregion

        Establish context = () =>
        {
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterType<TestSubscriber>().AsImplementedInterfaces();

            var container = builder.Build();

            Subject = new ContainerSubscriptionSource(container);
        };

        private Because of = () => subscriptions = Subject.GetSubscriptions();

        private It should_return_one_subscription = () => subscriptions.Should().HaveCount(1);

        private It should_return_async_subscription = () => subscriptions.Single().IsAsync.Should().BeTrue();

        private It should_return_event_subscription = () => subscriptions.Single().Should().BeOfType<EventSubscription>();

        private static IEnumerable<ISubscription> subscriptions;
    }


    [Subject(typeof(ContainerSubscriptionSource))]
    public class when_found_IConsume_of_Command_subscription : WithSubject<ContainerSubscriptionSource>
    {
        #region subscriber stub

        public class TestCommand : Command
        {
        }

        public class TestSubscriber : IConsume<TestCommand>
        {
            public void Consume(TestCommand message)
            {
                
            }
        }

        #endregion

        Establish context = () =>
        {
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterType<TestSubscriber>().AsImplementedInterfaces();

            var container = builder.Build();

            Subject = new ContainerSubscriptionSource(container);
        };

        private Because of = () => subscriptions = Subject.GetSubscriptions();

        private It should_return_one_subscription = () => subscriptions.Should().HaveCount(1);

        private It should_return_async_subscription = () => subscriptions.Single().IsAsync.Should().BeTrue();

        private It should_return_command_subscription = () => subscriptions.Single().Should().BeOfType<CommandSubscription>();

        private static IEnumerable<ISubscription> subscriptions;
    }
}
