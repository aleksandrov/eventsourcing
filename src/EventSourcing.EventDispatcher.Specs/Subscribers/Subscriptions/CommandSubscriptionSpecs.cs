﻿using EventSourcing.EventDispatcher.Subscribers.Subscriptions;
using FluentAssertions;
using Machine.Specifications;

namespace EventSourcing.EventDispatcher.Specs.Subscribers.Subscriptions
{
    [Subject(typeof(CommandSubscription))]
    class when_async_command_subscription_created 
    {
        class TestSubscriber
        {

        }

        class TestMessage
        {

        }


        Establish context = () => {  };

        private Because of = () => subscription = new CommandSubscription(typeof(TestSubscriber), typeof(TestMessage), true);

        private It should_have_Async_prefix_in_the_Id = () => subscription.GetId().Should().Be("Async" + typeof(TestSubscriber).Name);

        private static CommandSubscription subscription;
    }

    [Subject(typeof(CommandSubscription))]
    class when_sync_command_subscription_created
    {
        class TestSubscriber
        {

        }

        class TestMessage
        {

        }


        Establish context = () => { };

        private Because of = () => subscription = new CommandSubscription(typeof(TestSubscriber), typeof(TestMessage), false);

        private It should_have_Async_prefix_in_the_Id = () => subscription.GetId().Should().Be(typeof(TestSubscriber).Name);

        private static CommandSubscription subscription;
    }


}
