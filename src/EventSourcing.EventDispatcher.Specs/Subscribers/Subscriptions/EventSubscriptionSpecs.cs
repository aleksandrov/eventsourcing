﻿using EventSourcing.EventDispatcher.Subscribers.Subscriptions;
using FluentAssertions;
using Machine.Specifications;

namespace EventSourcing.EventDispatcher.Specs.Subscribers.Subscriptions
{
    [Subject(typeof(EventSubscription))]
    class when_event_subscription_created
    {
        class TestSubscriber
        {

        }

        class TestMessage
        {

        }


        Establish context = () => { };

        private Because of = () => subscription = new EventSubscription(typeof(TestSubscriber), typeof(TestMessage));

        private It should_have_Async_prefix_in_the_Id = () => subscription.GetId().Should().Be("Async" + typeof(TestSubscriber).Name);

        private static EventSubscription subscription;
    }

}
