﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using EventSourcing.Cqrs.Messages;
using EventSourcing.Cqrs.Messages.Commands;
using EventSourcing.EventDispatcher.Endpoints;
using EventSourcing.EventDispatcher.Routing;
using EventSourcing.EventDispatcher.Subscribers;
using EventSourcing.EventDispatcher.Subscribers.Subscriptions;
using EventSourcing.Interfaces;
using FluentAssertions;
using Machine.Fakes;
using Machine.Specifications;
using NSubstitute;

namespace EventSourcing.EventDispatcher.Specs.Routing
{
    [Subject(typeof(RoutingTable))]
    class when_loading_a_subscriber : RoutingTableSpecs
    {
        Establish context = () =>
        {
            var subscriptions = new List<ISubscription>
                {
                    new EventSubscription(typeof (TestEventHandler1), typeof (TestEvent1))
                };

            The<ISubscriptionSource>().GetSubscriptions().Returns(subscriptions);
        };

        private Because of = () => endpoints = Subject.GetEndpoints(new TestEvent1());

        private It should_call_endpointFactory_to_create_endpoint_for_each_subscriber = () => endpoints.Should().HaveCount(1);

        private static IEnumerable<IEndpoint> endpoints;
    }

    [Subject(typeof(RoutingTable))]
    class when_loading_same_subscriber_for_different_messages : RoutingTableSpecs
    {
        Establish context = () =>
        {
            var subscriptions = new List<ISubscription>
                {
                    new EventSubscription(typeof (TestEventHandler1), typeof (TestEvent1)),
                    new EventSubscription(typeof (TestEventHandler1), typeof (TestEvent2))
                };
            The<ISubscriptionSource>().GetSubscriptions().Returns(subscriptions);
        };

        private Because of = () =>
        {
            endpoint1 = Subject.GetEndpoints(new TestEvent1()).Single();
            endpoint2 = Subject.GetEndpoints(new TestEvent2()).Single();
            
        };

        private It should_reuse_endpoint_for_all_subscriptions = () => endpoint1.Should().BeSameAs(endpoint2);

        private static IEndpoint endpoint1;
        private static IEndpoint endpoint2;
    }

    [Subject(typeof(RoutingTable))]
    class when_loading_different_subscribers_for_same_event : RoutingTableSpecs
    {
        Establish context = () =>
        {
            var subscriptions = new List<ISubscription>
                {
                    new EventSubscription(typeof (TestEventHandler1), typeof (TestEvent1)),
                    new EventSubscription(typeof (TestEventHandler2), typeof (TestEvent1))
                };
            The<ISubscriptionSource>().WhenToldTo(s => s.GetSubscriptions()).Return(subscriptions);

            The<IEndpointFactory>().CreateProjectionEndpoint(Arg.Any<EventSubscription>()).Returns(new TestEndpoint { Marker = 1 }, new TestEndpoint { Marker = 2 });
        };

        private Because of = () => endpoints = Subject.GetEndpoints(new RoutingTableSpecs.TestEvent1()).ToList();

        private It should_return_two_endpoints_for_one_event = () =>
        {
            endpoints.Should().HaveCount(2);
            endpoints[0].Should().NotBeSameAs(endpoints[1]);
        };

        private It should_call_endpoint_factory_for_each_subscriber = () => The<IEndpointFactory>().Received(2).CreateProjectionEndpoint(Arg.Any<EventSubscription>());   

        private static List<IEndpoint> endpoints;
    }

    [Subject(typeof(RoutingTable))]
    class when_async_subscription_registered_for_the_command : RoutingTableSpecs
    {
        #region stub subscriber

        public class TestCommandConsumer : IConsume<TestCommand>
        {
            public void Consume(TestCommand message)
            {

            }
        }

        #endregion

        Establish context = () =>
        {
            subscriptions = new List<ISubscription>();
            subscriptions.Add(new CommandSubscription(typeof(TestCommandConsumer), typeof(TestCommand), isAsync: true));
            The<ISubscriptionSource>().GetSubscriptions().Returns(subscriptions);
            The<IEndpointFactory>().CreateCommandHandlerEndpoint(Arg.Is<CommandSubscription>(s => s.IsAsync)).Returns(The<IEndpoint>());
        };

        private Because of = () => Subject.GetEndpoints(new TestCommand());

        private It should_call_factory_to_create_async_endpoint = () => The<IEndpointFactory>().Received().CreateCommandHandlerEndpoint(Arg.Is<CommandSubscription>(s => s.IsAsync));

        private static List<ISubscription> subscriptions;
    }

    [Subject(typeof(RoutingTable))]
    class when_one_type_implements_consumption_of_command_and_event : RoutingTableSpecs
    {
        #region stub subscriber

        public class TestCommandConsumer : IConsume<TestCommand>, IConsume<TestEvent1>
        {
            public void Consume(TestCommand message)
            {

            }

            public void Consume(TestEvent1 message)
            {
                
            }
        }

        #endregion

        Establish context = () =>
        {
            subscriptions = new List<ISubscription>();
            subscriptions.Add(new CommandSubscription(typeof(TestCommandConsumer), typeof(TestCommand), isAsync: true));
            subscriptions.Add(new EventSubscription(typeof(TestCommandConsumer), typeof(TestEvent1)));
            The<ISubscriptionSource>().GetSubscriptions().Returns(subscriptions);

            The<IEndpointFactory>().CreateCommandHandlerEndpoint(Arg.Is<CommandSubscription>(s => s.IsAsync)).Returns(The<IEndpoint>());


        };

        private Because of = () =>
        {
            commandEndpoints = Subject.GetEndpoints(new TestCommand());
            eventEndpoints = Subject.GetEndpoints(new TestEvent1()); 
        };

        private It should_create_single_endpoint = () => commandEndpoints.Should().BeEquivalentTo(eventEndpoints);

        private static List<ISubscription> subscriptions;
        private static IEnumerable<IEndpoint> commandEndpoints;
        private static IEnumerable<IEndpoint> eventEndpoints;
    }

    [Subject(typeof(RoutingTable))]
    class when_one_type_mix_handling_of_command_and_consuption_of_event : RoutingTableSpecs
    {
        #region stub subscriber

        public class TestCommandConsumer : IHandles<TestCommand>, IConsume<TestEvent1>
        {
            public ExecutionResult Handle(TestCommand message)
            {
                return ExecutionResult.Empty;
            }

            public void Consume(TestEvent1 message)
            {

            }
        }

        #endregion


        Establish context = () =>
        {
            subscriptions = new List<ISubscription>();
            subscriptions.Add(new CommandSubscription(typeof(TestCommandConsumer), typeof(TestCommand)));
            subscriptions.Add(new EventSubscription(typeof(TestCommandConsumer), typeof(TestEvent1)));
            The<ISubscriptionSource>().GetSubscriptions().Returns(subscriptions);

            The<IEndpointFactory>().CreateCommandHandlerEndpoint(Arg.Is<CommandSubscription>(s => s.IsAsync)).Returns(The<IEndpoint>());
        };

        private Because of = () =>
        {
            commandEndpoints = Subject.GetEndpoints(new TestCommand());
            eventEndpoints = Subject.GetEndpoints(new TestEvent1()); 
        };

        private It should_create_two_endpoints = () => commandEndpoints.Single().Should().NotBe(eventEndpoints.Single());

        private static List<ISubscription> subscriptions;
        private static IEnumerable<IEndpoint> commandEndpoints;
        private static IEnumerable<IEndpoint> eventEndpoints;
    }

    [Subject(typeof(RoutingTable))]
    class when_adding_null_subscription_source : RoutingTableSpecs
    {
        Establish context = () => {  };

        private Because of = () => action = () => Subject.AddSubscriptionSource(null);

        private It should_raise_an_error = () => action.ShouldThrow<ArgumentNullException>();

        private static Action action;
    }

    [Subject(typeof(RoutingTable))]
    class when_adding_subscription_source_with_given_subscription : RoutingTableSpecs
    {
        Establish context = () =>
        {
            source = An<ISubscriptionSource>();
            var subscriptions = new List<ISubscription>
                {
                    new EventSubscription(typeof (TestEventHandler1), typeof (TestEvent1))
                };
            source.GetSubscriptions().Returns(subscriptions);
        };

        private Because of = () => Subject.AddSubscriptionSource(source);

        private It should_create_endpoint_for_the_given_subscription = () => { };

        private static ISubscriptionSource source;
    }

    public class RoutingTableSpecs : WithSubject<RoutingTable>
    {
        #region stubs

        public class TestEvent1 : Cqrs.Messages.Events.IEvent
        {
            public DateTime Created { get; set; }

            public int Version { get; set; }
            public Guid CorrelationId { get; set; }
        }

        public class TestEvent2 : Cqrs.Messages.Events.IEvent
        {
            public DateTime Created { get; set; }

            public int Version { get; set; }
            public Guid CorrelationId { get; set; }
        }

        public class TestCommand : Command
        {
            public DateTime Created { get; set; }
        }

        public class TestEventHandler1 : IConsume<TestEvent1>
        {
            public bool IsFired;

            public void Consume(TestEvent1 message)
            {
                IsFired = true;
            }
        }

        public class TestEventHandler2 : IConsume<TestEvent1>
        {
            public bool IsFired;

            public void Consume(TestEvent1 message)
            {
                IsFired = true;
            }
        }

        public class TestCommandHandlerOne : IHandles<TestCommand>
        {
            public ExecutionResult Handle(TestCommand message)
            {
                throw new NotImplementedException();
            }
        }

        public class TestCommandHandlerTwo : IHandles<TestCommand>
        {
            public ExecutionResult Handle(TestCommand message)
            {
                throw new NotImplementedException();
            }
        }

        public class TestEndpoint : IEndpoint
        {
            public int Marker { get; set; }

            public ExecutionResult Handle(IMessage e, ILifetimeScope scope)
            {
                throw new NotImplementedException();
            }

            public int UnprocessedEventsCount { get; private set; }
            public string Name { get; private set; }
        }

        #endregion

    }
}
