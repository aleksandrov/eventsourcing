﻿using System;
using EventSourcing.Cqrs.Messages.Events;

namespace EventSourcing.Rebuild.Messages.Events
{
    public class RebuildStarted : Event
    {
    }
}
