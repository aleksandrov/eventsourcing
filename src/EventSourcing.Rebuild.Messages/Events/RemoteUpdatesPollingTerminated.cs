﻿using EventSourcing.Cqrs.Messages.Events;

namespace EventSourcing.Rebuild.Messages.Events
{
    public class RemoteUpdatesPollingTerminated : Event
    {
        public string[] ProjectionNames { get; set; }

        public string[] Buckets { get; set; }

        public bool RevertProjectionsSelection { get; set; }

        public string Error { get; set; }

        public string StackTrace { get; set; }

        public RemoteUpdatesPollingTerminated(string[] projectionNames, string[] buckets, bool revertProjectionsSelection, string error, string stackTrace)
        {
            ProjectionNames = projectionNames;
            Buckets = buckets;
            RevertProjectionsSelection = revertProjectionsSelection;
            Error = error;
            StackTrace = stackTrace;
        }
    }
}
