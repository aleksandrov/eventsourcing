﻿using EventSourcing.Cqrs.Messages.Events;

namespace EventSourcing.Rebuild.Messages.Events
{
    public class CommitFound : Event
    {
        public string Checkpoint { get; set; }

        public CommitFound(string checkpoint)
        {
            Checkpoint = checkpoint;
        }
    }
}
