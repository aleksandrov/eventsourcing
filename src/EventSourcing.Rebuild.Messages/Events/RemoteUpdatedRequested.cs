﻿using EventSourcing.Cqrs.Messages.Events;

namespace EventSourcing.Rebuild.Messages.Events
{
    public class RemoteUpdatedRequested : Event
    {
        public string[] ProjectionNames { get; set; }

        public string[] Buckets { get; set; }

        public bool RevertProjectionsSelection { get; set; }

        public RemoteUpdatedRequested(string[] projectionNames, string[] buckets, bool revertProjectionsSelection)
        {
            ProjectionNames = projectionNames;
            Buckets = buckets;
            RevertProjectionsSelection = revertProjectionsSelection;
        }
    }
}
