﻿using EventSourcing.Cqrs.Messages.Events;

namespace EventSourcing.Rebuild.Messages.Events
{
    public class CheckpointChanged : Event
    {
        public string Checkpoint { get; set; }

        public CheckpointChanged(string checkpoint)
        {
            Checkpoint = checkpoint;
        }
    }
}
