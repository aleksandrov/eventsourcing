﻿using System;
using EventSourcing.Cqrs.Messages.Commands;

namespace EventSourcing.Rebuild.Messages.Commands
{
	public class RebuildProjectionsCommand : ICommand
	{
	    public Guid CorrelationId { get; set; }

        public string[] ProjectionNames { get; set; }

        public bool RevertProjectionsSelection { get; set; }

        public string[] Buckets { get; set; }

	    public Guid Id { get; set; }

	}
}