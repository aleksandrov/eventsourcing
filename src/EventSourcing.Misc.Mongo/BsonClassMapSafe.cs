﻿using System;
using MongoDB.Bson.Serialization;

namespace EventSourcing.Misc.Mongo
{
    public static class BsonClassMapSafe
    {
        private static readonly object lockObject = new object();

        public static void RegisterClassMap<TClass>()
        {
            lock (lockObject)
            {
                if (!BsonClassMap.IsClassMapRegistered(typeof (TClass)))
                {
                    BsonClassMap.RegisterClassMap<TClass>();
                }
            }
        }

        public static void RegisterClassMap(BsonClassMap classMap)
        {
            lock (lockObject)
            {
                if (!BsonClassMap.IsClassMapRegistered(classMap.ClassType))
                {
                    BsonClassMap.RegisterClassMap(classMap);
                }
            }
        }

        public static void RegisterClassMap<TClass>(Action<BsonClassMap<TClass>> classMapInitializer)
        {
            lock (lockObject)
            {
                if (!BsonClassMap.IsClassMapRegistered(typeof (TClass)))
                {
                    BsonClassMap.RegisterClassMap(classMapInitializer);
                }
            }
        }
    }
}
