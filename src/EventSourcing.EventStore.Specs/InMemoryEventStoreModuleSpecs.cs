﻿using Autofac;
using EventSourcing.Cqrs;
using EventSourcing.EventDispatcher;
using EventSourcing.Interfaces.CommonDomain.Persistence;
using EventSourcing.Logging;
using FluentAssertions;
using Machine.Specifications;

namespace EventSourcing.EventStore.Specs
{
    public class InMemoryEventStoreModuleSpecs
    {
        Establish context = () =>
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule(new CqrsModule());
            builder.RegisterModule(new Log4NetModule());
            builder.RegisterModule(new EventDispatcherModule());
            builder.RegisterModule(new InMemoryEventStoreModule("test", "test"));
            container = builder.Build();
        };

        Because of = () => result = container.ResolveNamed<IRepository>("test");

        private It should_return_repository = () => result.Should().NotBeNull();

        private static IContainer container;
        private static IRepository result;
    }
}
