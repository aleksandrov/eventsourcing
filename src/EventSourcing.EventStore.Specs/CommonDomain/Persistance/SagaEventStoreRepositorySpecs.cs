﻿using System;
using EventSourcing.CommonDomain.Core;
using EventSourcing.Cqrs.Messages;
using EventSourcing.Cqrs.Messages.Events;
using EventSourcing.EventStore.CommonDomain.Persistence;
using EventSourcing.EventStore.CommonDomain.Persistence.EventStore;
using EventSourcing.Interfaces.CommonDomain;
using FluentAssertions;
using Machine.Fakes;
using Machine.Specifications;
using NEventStore;

namespace EventSourcing.EventStore.Specs.CommonDomain.Persistance
{

    [Subject(typeof(SagaEventStoreRepository))]
    class when_loading_incorrect_saga_inplementation_for_existing_event_stream : SagaEventStoreRepositorySpecs
    {
        Establish context = () =>
        {
            sagaId = Guid.NewGuid();

            repo = new SagaEventStoreRepository(Subject, new SagaFactory());
            var saga = repo.GetById<Saga1>(sagaId);
            saga.Transition(new CommonEvent() { Value = "test"});
            repo.Save(saga, Guid.NewGuid(), headers => { });

            repo = new SagaEventStoreRepository(Subject, new SagaFactory());
        };

        private Because of = () => saga = repo.GetById<Saga2>(sagaId);

        private It should_return_null_saga = () => saga.Should().BeOfType<NullSaga>();

        private static Guid sagaId;
        private static SagaEventStoreRepository repo;

        private static ISaga saga;
    }

    class SagaEventStoreRepositorySpecs : WithSubject<IStoreEvents>
    {
        #region saga factory

        public class SagaFactory : IConstructSagas
        {
            public TSaga Build<TSaga>(Guid id) where TSaga : class, ISaga
            {
                return (TSaga) Activator.CreateInstance(typeof (TSaga), id);
            }
        }


        #endregion

        #region sagas

        public class Saga1 : SagaBase<IMessage>
        {
            private bool flag;

            public Saga1(Guid id)
            {
                Id = id;
                Register<CommonEvent>(Apply);
                
            }

            private void Apply(CommonEvent message)
            {
                flag = true;
            }
        }

        public class Saga2 : SagaBase<IMessage>
        {
            private bool flag;

            public Saga2(Guid id)
            {
                Id = id;
                Register<CommonEvent>(Apply);
            }

            private void Apply(CommonEvent message)
            {
                flag = true;
            }
        }

#endregion

        #region events

        public class CommonEvent : Event
        {
            public string Value { get; set; }
        }

        public class TestEvent2 : Event
        {
            public string Name { get; set; }
        }

        #endregion

        Establish context = () =>
        {
            Subject = Wireup.Init().UsingInMemoryPersistence().Build();
        };
    }
}
