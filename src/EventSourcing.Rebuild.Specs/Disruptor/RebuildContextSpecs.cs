﻿using System;
using System.Collections.Generic;
using Autofac;
using Disruptor;
using EventSourcing.Cqrs.Messages.Events;
using EventSourcing.Interfaces.History;
using EventSourcing.Interfaces.Services;
using EventSourcing.Rebuild.Disruptor;
using EventSourcing.Rebuild.Messages.Events;
using EventSourcing.Rebuild.Services;
using EventSourcing.Rebuild.Services.Checkpoints;
using FluentAssertions;
using Machine.Fakes;
using Machine.Specifications;
using NEventStore;
using NSubstitute;

namespace EventSourcing.Rebuild.Specs.Disruptor
{

    [Subject(typeof(RebuildContext))]
    class when_getting_rebuild_result_for_unused_context : RebuildContextSpecs
    {
        Establish context = () => {  };

        private Because of = () => action = () => Subject.GetRebuildResult();

        private It should_should = () => action.ShouldThrow<ApplicationException>().WithMessage("No rebuild result was generated.");

        private static Action action;
    }

    [Subject(typeof(RebuildContext))]
    class when_rebuild_started : RebuildContextSpecs
    {
        Establish context = () =>
        {
            projection1 = An<IRebuildable>();
            projection2 = An<IRebuildable>();
            The<IRebuildsProjections>().GetRebuildableProjections(projectionNames, false).Returns(new[] { projection1, projection2 });
        };

        private Because of = () =>
        {
            Subject.BeginRebuild();
            Subject.Dispose();
        };

        private It should_send_RebuildStarted_to_projection1_endpoint = () => projection1.Received().Rebuild(Arg.Is<IEvent>(e => e is RebuildStarted), Arg.Any<ILifetimeScope>());

        private It should_send_RebuildStarted_to_projection2_endpoint = () => projection1.Received().Rebuild(Arg.Is<IEvent>(e => e is RebuildStarted), Arg.Any<ILifetimeScope>());

        private static IRebuildable projection1;
        private static IRebuildable projection2;
    }

    [Subject(typeof(RebuildContext))]
    class when_rebuild_finished : RebuildContextSpecs
    {
        Establish context = () =>
        {
            projection1 = An<IRebuildable>();
            projection2 = An<IRebuildable>();
            The<IRebuildsProjections>().GetRebuildableProjections(projectionNames, false).Returns(new[] { projection1, projection2 });
        };

        private Because of = () =>
        {
            Subject.EndRebuild();
            Subject.Dispose();
        };

        private It should_send_RebuildStarted_to_projection1_endpoint = () => projection1.Received().Rebuild(Arg.Is<IEvent>(e => e is RebuildFinished), Arg.Any<ILifetimeScope>());

        private It should_send_RebuildStarted_to_projection2_endpoint = () => projection1.Received().Rebuild(Arg.Is<IEvent>(e => e is RebuildFinished), Arg.Any<ILifetimeScope>());

        private static IRebuildable projection1;
        private static IRebuildable projection2;
    }

    [Subject(typeof(RebuildContext))]
    class when_executing_for_the_commit_and_need_to_write_checkpoint : RebuildContextSpecs
    {
        Establish context = () =>
        {
            testEvent = new TestEvent();

            The<ICommit>().Events.Returns(new[] { new EventMessage() { Body = testEvent } });
            The<ICommit>().CheckpointToken.Returns("token");

            projection1 = An<IRebuildable>();
            projection2 = An<IRebuildable>();
            The<IRebuildsProjections>().GetRebuildableProjections(projectionNames, false).Returns(new[] { projection1, projection2 });

            
        };

        private Because of = () =>
        {
            Subject.Process(The<ICommit>());
            Subject.Dispose();
        };

        private It should_call_Header_Manager_to_update_event_from_headers = () => The<IHeaderManager>().Received().UpdateEvent(Arg.Is<TestEvent>(e => e == testEvent), Arg.Any<IDictionary<string, object>>());

        //private It should_call_publisher_inform_about_the_latest_checkpoint = () => The<IEventPublisher>().Received().Publish(Arg.Is<CheckpointChanged>(e => e.Checkpoint == "token"));

        private It should_rebuild_projection1 = () => projection1.Received().Rebuild(testEvent, Arg.Any<ILifetimeScope>());

        private It should_rebuild_projection2 = () => projection2.Received().Rebuild(testEvent, Arg.Any<ILifetimeScope>());

        private It should_call_Wait_Strategy_Factory_to_get_Wait_Strategy = () => The<IWaitStrategyFactory>().Received().CreateWaitStrategy();

        private It should_call_services_in_correct_order = () =>
        {
            Received.InOrder(() =>
            {
                The<IHeaderManager>().UpdateEvent(Arg.Is<TestEvent>(e => e == testEvent), Arg.Any<IDictionary<string, object>>());
                projection1.Rebuild(testEvent, Arg.Any<ILifetimeScope>());
                The<IStoreCheckpoint>().SaveCheckpoint("token");
                //The<IEventPublisher>().Publish(Arg.Any<CheckpointChanged>());
            });

            Received.InOrder(() =>
            {
                The<IHeaderManager>().UpdateEvent(Arg.Is<TestEvent>(e => e == testEvent), Arg.Any<IDictionary<string, object>>());
                projection2.Rebuild(testEvent, Arg.Any<ILifetimeScope>());
                The<IStoreCheckpoint>().SaveCheckpoint("token");
                //The<IEventPublisher>().Publish(Arg.Any<CheckpointChanged>());
            });
        };

        private static TestEvent testEvent;
        private static IRebuildable projection1;
        private static IRebuildable projection2;
    }

    class RebuildContextSpecs : WithSubject<RebuildContext>
    {

        #region test event

        public class TestEvent : IEvent
        {
            public string Value { get; set; }
            public Guid CorrelationId { get; set; }
            public DateTime Created { get; set; }
            public int Version { get; set; }
        }

        #endregion


        Establish context = () =>
        {
            buckets = null;
            projectionNames = null;

            The<IWaitStrategyFactory>().CreateWaitStrategy().Returns(new BlockingWaitStrategy());

            var builder = new ContainerBuilder();
            builder.RegisterModule(new DisruptorModule());
            builder.Register(ctx => The<IHeaderManager>()).As<IHeaderManager>();
            builder.Register(ctx => The<IEventPublisher>()).As<IEventPublisher>();
            builder.Register(ctx => The<IStoreCheckpoint>()).As<IStoreCheckpoint>();
            builder.Register(ctx => The<ILogger>()).As<ILogger>();

            container = builder.Build();
            Subject = new RebuildContext(container, The<IWaitStrategyFactory>(), 1024, The<IRebuildsProjections>(), The<ILogger>(), buckets, projectionNames, revertSelection: false, writeCheckpoint: true);
        };

        protected static IContainer container;
        protected static string[] projectionNames;
        protected static string[] buckets;
    }
}
