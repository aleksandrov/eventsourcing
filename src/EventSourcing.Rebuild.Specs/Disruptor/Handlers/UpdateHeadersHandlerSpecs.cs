﻿using System;
using System.Collections.Generic;
using EventSourcing.Cqrs.Messages.Events;
using EventSourcing.Interfaces.Services;
using EventSourcing.Rebuild.Disruptor.Handlers;
using EventSourcing.Rebuild.Disruptor.Messages;
using Machine.Fakes;
using Machine.Specifications;
using NEventStore;
using NSubstitute;

namespace EventSourcing.Rebuild.Specs.Disruptor.Handlers
{
    [Subject(typeof(ParseHeadersHandler))]
    class when_new_Commit_found : UpdateHeadersHandlerSpecs
    {
        Establish context = () =>
        {
            e = new TestEvent();

            data = new CommitMessage();
            data.Commit = The<ICommit>();
            data.Commit.Events.Returns(new[] {new EventMessage() {Body = e}});
        };

        private Because of = () => Subject.OnNext(data, 123, false);

        private It should_call_header_manager_to_update_events = () => The<IHeaderManager>().Received().UpdateEvent(Arg.Any<IEvent>(), Arg.Any<IDictionary<string, object>>());

        private static CommitMessage data;
        private static TestEvent e;
    }

    class UpdateHeadersHandlerSpecs : WithSubject<ParseHeadersHandler>
    {
        public class TestEvent : IEvent
        {
            public Guid CorrelationId { get; set; }

            public DateTime Created { get; set; }

            public int Version { get; set; }
        }
    }
}
