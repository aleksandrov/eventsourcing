﻿using EventSourcing.Interfaces.Services;
using EventSourcing.Rebuild.Disruptor.Handlers;
using EventSourcing.Rebuild.Disruptor.Messages;
using EventSourcing.Rebuild.Messages.Events;
using Machine.Fakes;
using Machine.Specifications;
using NEventStore;
using NSubstitute;

namespace EventSourcing.Rebuild.Specs.Disruptor.Handlers
{
    [Subject(typeof(LogCheckpointHandler))]
    class when_logging_checkpoint : LogCheckpointHandlerSpecs
    {
        Establish context = () =>
        {
            The<ICommit>().CheckpointToken.Returns("123");

            data = new CommitMessage();
            data.Commit = The<ICommit>();            
        };

        private Because of = () => Subject.OnNext(data, 123456, true);

        private It should_call_publisher_inform_about_the_latest_checkpoint = () => The<IEventPublisher>().Received().Publish(Arg.Is<CheckpointChanged>(e => e.Checkpoint == "123"));

        private static CommitMessage data;
    }


    class LogCheckpointHandlerSpecs : WithSubject<LogCheckpointHandler>
    {
    }
}
