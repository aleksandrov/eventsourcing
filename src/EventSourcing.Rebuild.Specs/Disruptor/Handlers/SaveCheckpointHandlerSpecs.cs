﻿using EventSourcing.Rebuild.Disruptor.Handlers;
using EventSourcing.Rebuild.Disruptor.Messages;
using EventSourcing.Rebuild.Services.Checkpoints;
using Machine.Fakes;
using Machine.Specifications;
using NEventStore;
using NSubstitute;

namespace EventSourcing.Rebuild.Specs.Disruptor.Handlers
{
    [Subject(typeof(WriteCheckpointHandler))]
    class when_processing_Commit : SaveCheckpointHandlerSpecs
    {
        Establish context = () =>
        {
            The<ICommit>().CheckpointToken.Returns("123");

            data = new CommitMessage();
            data.Commit = The<ICommit>();
        };

        private Because of = () => Subject.OnNext(data, 123456, true);

        private It should_save_processed_checkpoint = () => The<IStoreCheckpoint>().Received().SaveCheckpoint(Arg.Is<string>(x => x == "123"));

        private static CommitMessage data;
    }

    class SaveCheckpointHandlerSpecs : WithSubject<WriteCheckpointHandler>
    {
    }
}
