﻿using Autofac;
using Autofac.Builder;
using Autofac.Core;
using EventSourcing.Interfaces;
using EventSourcing.Interfaces.CommonDomain.Persistence;

namespace EventSourcing.Cqrs.Autofac
{
	public static class RegistrationExtensions
	{
		public static IRegistrationBuilder<TProjectionImplementation, ConcreteReflectionActivatorData, SingleRegistrationStyle> 
            RegisterProjection<TProjectionImplementation, TProjectionContract>(this ContainerBuilder builder) where TProjectionImplementation : TProjectionContract
		{
			return builder.RegisterType<TProjectionImplementation>()
				.As<TProjectionContract>()
				.AsImplementedInterfaces()
				.AsSelf()
				.SingleInstance();
		}

        public static IRegistrationBuilder<TProjectionImplementation, ConcreteReflectionActivatorData, SingleRegistrationStyle>
            RegisterProjection<TProjectionImplementation>(this ContainerBuilder builder)
        {
            return builder.RegisterType<TProjectionImplementation>()
                .AsImplementedInterfaces()
                .AsSelf()
                .SingleInstance();
        }

        public static IRegistrationBuilder<THandler, ConcreteReflectionActivatorData, SingleRegistrationStyle>
            RegisterHandler<THandler>(this ContainerBuilder builder, string contextName)
        {
            var regionstation = builder.RegisterType<THandler>().AsSelf().AsImplementedInterfaces().InstancePerLifetimeScope();

            if (contextName != null)
            {
                regionstation.WithParameter(ResolvedParameter.ForNamed<IRepository>(contextName))
                             .WithParameter(ResolvedParameter.ForNamed<ISagaRepository>(contextName));
            }

            return regionstation;
        }

        public static IRegistrationBuilder<TSaga, ConcreteReflectionActivatorData, SingleRegistrationStyle> RegisterSaga<TSaga>(this ContainerBuilder builder)
        {
            return builder.RegisterType<TSaga>().AsImplementedInterfaces().AsSelf();
        }


	    public static IRegistrationBuilder<TEntity, ConcreteReflectionActivatorData, SingleRegistrationStyle> RegisterAggregateRoot<TEntity>(this ContainerBuilder builder)
	    {
            return builder.RegisterType<TEntity>().AsSelf().AsImplementedInterfaces();
	    }

        public static IRegistrationBuilder<THandler, ConcreteReflectionActivatorData, SingleRegistrationStyle>
            RegisterHandler<THandler>(this ContainerBuilder builder)
        {

            var regionstation = builder.RegisterType<THandler>().AsSelf().AsImplementedInterfaces().InstancePerLifetimeScope();

            return regionstation;
        }
	}
}
