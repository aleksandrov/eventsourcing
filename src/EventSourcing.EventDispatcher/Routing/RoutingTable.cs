﻿using System;
using System.Collections.Generic;
using System.Linq;
using EventSourcing.Cqrs.Messages;
using EventSourcing.EventDispatcher.Endpoints;
using EventSourcing.EventDispatcher.Subscribers;
using EventSourcing.Interfaces.History;
using EventSourcing.Interfaces.Services;

namespace EventSourcing.EventDispatcher.Routing
{
    public interface IRoutingTable
    {
        IEnumerable<IEndpoint> GetEndpoints(IMessage message);
        IEnumerable<IRebuildable> GetRebuildableEndpoints();
        IEnumerable<IEndpoint> GetEndpoints();
        void AddSubscriptionSource(ISubscriptionSource source);
    }

    public class RoutingTable : IRoutingTable
    {
        private readonly IList<ISubscriptionSource> sources;
        private readonly IEndpointFactory endpointFactory;
        private readonly ILogger logger;

        private ILookup<Type, IEndpoint> lookup;
        private readonly IDictionary<string, IEndpoint> endpointsMap;

        public RoutingTable(ISubscriptionSource source, IEndpointFactory endpointFactory, ILogger logger)
        {
            if (source == null) throw new ArgumentNullException("source");
            if (endpointFactory == null) throw new ArgumentNullException("endpointFactory");
            if (logger == null) throw new ArgumentNullException("logger");

            sources = new List<ISubscriptionSource>();
            sources.Add(source);

            this.endpointFactory = endpointFactory;
            this.logger = logger;
            endpointsMap = new Dictionary<string, IEndpoint>();
        }

        private void EnsureLookups()
        {
            if (lookup != null) return;

            lookup = sources.SelectMany(s => s.GetSubscriptions()).ToLookup(subscription => subscription.EventType, subscription =>
            {
                var endpoint = GetEndpoint(subscription);
                logger.DebugFormat("Endpoint {0} subscribed to {1}", subscription.GetId(), subscription.EventType.Name);
                return endpoint;
            });
        }

        private void InvalidateLookups()
        {
            lookup = null;
            endpointsMap.Clear();
        }

        public IEnumerable<IEndpoint> GetEndpoints(IMessage message)
        {
            if (message == null) throw new ArgumentNullException("message");
            EnsureLookups();

            return lookup[message.GetType()];
        }

        private IEndpoint GetEndpoint(ISubscription subscription)
        {
            if (!endpointsMap.ContainsKey(subscription.GetId()))
            {
                var endpoint = subscription.CreateEndpoint(endpointFactory);
                endpointsMap.Add(subscription.GetId(), endpoint);

                logger.DebugFormat("Created {1} for: {0}", subscription.GetId(), endpoint.GetType().Name);
            }

            return endpointsMap[subscription.GetId()];
        }

        public IEnumerable<IRebuildable> GetRebuildableEndpoints()
        {
            EnsureLookups();

            return endpointsMap.Where(pair => pair.Value is IRebuildable).Select(pair => pair.Value as IRebuildable);
        }

        public IEnumerable<IEndpoint> GetEndpoints()
        {
            EnsureLookups();

            return endpointsMap.Select(pair => pair.Value);
        }

        public void AddSubscriptionSource(ISubscriptionSource source)
        {
            if (source == null) throw new ArgumentNullException("source");

            sources.Add(source);
            InvalidateLookups();
            EnsureLookups();
        }
    }
}
