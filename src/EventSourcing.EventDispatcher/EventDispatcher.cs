﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using EventSourcing.Cqrs.Messages;
using EventSourcing.Cqrs.Messages.Commands;
using EventSourcing.Cqrs.Messages.Events;
using EventSourcing.EventDispatcher.Routing;
using EventSourcing.Interfaces.History;
using EventSourcing.Interfaces.Services;

namespace EventSourcing.EventDispatcher
{
    public class EventDispatcher : IEventPublisher, IRebuildsProjections, ICommandSender
    {
        private readonly IRoutingTable routing;
        private readonly ILifetimeScope dispatcherScope;

        readonly object lockObject = new object();

        public EventDispatcher(IRoutingTable routing, ILifetimeScope dispatcherScope)
        {
            this.routing = routing;
            this.dispatcherScope = dispatcherScope;
        }

        public void Publish(IEvent @event)
        {
            var endpoints = routing.GetEndpoints(@event).ToList();
            lock (lockObject)
            {
                foreach (var endpoint in endpoints)
                {
                    endpoint.Handle(@event, dispatcherScope);
                }
            }
        }

        public void Publish(IEnumerable<IEvent> events)
        {
            lock (lockObject)
            {
                foreach (var e in events)
                {
                    var endpoints = routing.GetEndpoints(e).ToList();
                    foreach (var endpoint in endpoints)
                    {
                        endpoint.Handle(e, dispatcherScope);
                    }
                }
            }
        }

        public IEnumerable<IRebuildable> GetRebuildableProjections(IEnumerable<string> projectionNames, bool revertSelection = false)
        {
            var endpoints = routing.GetRebuildableEndpoints();
            if (projectionNames != null)
            {
                if (revertSelection)
                {
                    endpoints = endpoints.Where(e => !projectionNames.Contains(e.Name));
                }
                else
                {
                    endpoints = endpoints.Where(e => projectionNames.Contains(e.Name));
                }
            }
            return endpoints;
        }

        public ExecutionResult Send(ICommand command)
        {
            if (command == null) throw new ArgumentNullException();
            

            if (command.Id == Guid.Empty)
            {
                command.Id = Guid.NewGuid();
            }

            var endpoints = routing.GetEndpoints(command).ToList();
            if (endpoints.Count == 0)
            {
                throw new InvalidOperationException(string.Format("No handler registered for command {0}", command.GetType().Name));
            }

            if (endpoints.Count > 1)
            {
                throw new InvalidOperationException(string.Format("More than one handler found for the command {0}.",  command.GetType().Name));
            }

            using (var commandScope = dispatcherScope.BeginLifetimeScope())
            {
                var result = endpoints[0].Handle(command, commandScope);
                return result;
            }

            
        }

    }
}
