﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllUnite.EventDispatcher.Queues
{
    public class ProcessingQueueBuilder<TMessage>
    {
        private string _name;
        private Action<TMessage> _processMessageAction;
        private Action<TMessage, Exception> _processErrorAction;

        public ProcessingQueueBuilder<TMessage> Named(string name)
        {
            if (name == null) throw new ArgumentNullException("name");
            _name = name;

            return this;
        }

        public ProcessingQueueBuilder<TMessage> ProcessWith(Action<TMessage> processMessageAction)
        {
            _processMessageAction = processMessageAction;
            return this;
        }

        public ProcessingQueueBuilder<TMessage> HandleErrorsWith(Action<TMessage, Exception> processErrorAction)
        {
            _processErrorAction = processErrorAction;
            return this;
        }

        public ProcessingQueue<TMessage> Build()
        {
            var result = new ProcessingQueue<TMessage>(_name, _processMessageAction, _processErrorAction);

            return result;
        }

        
    }

}
