﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AllUnite.Cqrs.Messages;

namespace AllUnite.EventDispatcher.Queues
{
    public class ProcessingQueue<TMessage>
    {
        private readonly Action<TMessage> _onProcessAction;
        private readonly Action<TMessage, Exception> _onErrorAction;
        private readonly BlockingCollection<TMessage> messageQueue;

        #region static

        public static ProcessingQueueBuilder<TMessage> Configure()
        {
            return new ProcessingQueueBuilder<TMessage>();
        }

        #endregion

        public ProcessingQueue(string name, Action<TMessage> onProcessAction, Action<TMessage, Exception> onErrorAction)
        {
            _onProcessAction = onProcessAction;
            _onErrorAction = onErrorAction;
            messageQueue = new BlockingCollection<TMessage>();
            messageQueue.GetConsumingEnumerable()
                .ToObservable(new NewThreadScheduler(action => new Thread(action) { IsBackground = true, Name = name }))
                .Subscribe(ProcessMessage);
        }

        public int QueueDepth
        {
            get
            {
                return messageQueue.Count;
            }
        }

        public void Enqueue(TMessage message)
        {
            messageQueue.Add(message);
        }

        private void ProcessMessage(TMessage message)
        {
            try
            {
                _onProcessAction(message);
            }
            catch (Exception ex)
            {
                _onErrorAction(message, ex);
            }
        }
    }
}
