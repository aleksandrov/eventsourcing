﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using Autofac;
using EventSourcing.Cqrs.Messages;
using EventSourcing.Interfaces;
using EventSourcing.Interfaces.Services;

namespace EventSourcing.EventDispatcher.Endpoints.Async
{
    public abstract class AsyncEndpointBase<TConsumer> : IAsyncEndpoint
    {
        private readonly ILifetimeScope endpointScope;
        private readonly ActionCache<TConsumer> actionCache;

        private readonly ILogger logger;
        private BlockingCollection<IMessage> messageQueue;
        private readonly Stopwatch watch;

        protected AsyncEndpointBase(ILifetimeScope endpointScope, ILogger logger)
        {
            this.endpointScope = endpointScope;
            actionCache = new ActionCache<TConsumer>();

            Name = typeof(TConsumer).Name;
            this.logger = logger;
            this.watch = new Stopwatch();
        }

        BlockingCollection<IMessage> GetQueue()
        {
            if (messageQueue == null)
            {
                messageQueue = new BlockingCollection<IMessage>();
                messageQueue.GetConsumingEnumerable()
                    .ToObservable(TaskPoolScheduler.Default)
                    .Subscribe(ProcessMessageInternal);
            }

            return messageQueue;
        }

        public string Name { get; private set; }

        public virtual ExecutionResult Handle(IMessage e, ILifetimeScope scope)
        {
            GetQueue().Add(e);
            logger.DebugFormat("Event {0} is scheduled for processing in {1}", e.GetType().Name, Name);
            return new ExecutionResult();
        }

        private void ProcessMessageInternal(IMessage e)
        {
            ProcessMesage(e, endpointScope);
        }

        protected virtual void ProcessMesage(IMessage e, ILifetimeScope scope)
        {
            watch.Start();
            try
            {
                var handler = scope.Resolve<TConsumer>();
                var handle = actionCache.GetCallAction(e.GetType());
                handle(handler, e);
            }
            catch (IncorrectSagaException ex)
            {
                logger.DebugFormat("Incorrect saga loaded: '{0}', expected '{1}'", ex.ActualType, ex.ExpectedType);
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Exception raised during processing '{0}' in '{1}': {2}", e.GetType().Name, Name, ex.Message);
            }
            finally
            {
                watch.Stop();
            }

            UpdateAverageProcessingTime(watch.Elapsed);
            UpdateMaxProcessingTime(watch.Elapsed);
            UpdateMinProcessingTime(watch.Elapsed);

            ProcessedEventsCount++;
            watch.Reset();
        }

        private void UpdateMinProcessingTime(TimeSpan current)
        {
            if (MinProcessingTime != TimeSpan.Zero)
            {
                if (current < MinProcessingTime)
                {
                    MinProcessingTime = current;
                }
            }
            else
            {
                MinProcessingTime = current;
            }
        }

        private void UpdateMaxProcessingTime(TimeSpan current)
        {
            if (current > MaxProcessingTime)
            {
                MaxProcessingTime = current;
            }
        }

        private void UpdateAverageProcessingTime(TimeSpan current)
        {
            AverageProcessingTime = TimeSpan.FromMilliseconds((AverageProcessingTime + current).TotalMilliseconds / 2);
        }

        #region diagnostics

        public int UnprocessedEventsCount
        {
            get { return messageQueue.Count; }
        }

        public long ProcessedEventsCount { get; private set; }
        public TimeSpan AverageProcessingTime { get; private set; }
        public TimeSpan MaxProcessingTime { get; private set; }
        public TimeSpan MinProcessingTime { get; private set; }

        #endregion

    }
}
