﻿using System;
using Autofac;
using EventSourcing.Cqrs.Messages;
using EventSourcing.Cqrs.Messages.Events;
using EventSourcing.Interfaces;
using EventSourcing.Interfaces.History;
using EventSourcing.Interfaces.Services;
using EventSourcing.Rebuild.Messages.Events;

namespace EventSourcing.EventDispatcher.Endpoints.Async
{
    public class AsyncConsumerEndpoint<TConsumer> : AsyncEndpointBase<TConsumer>, IRebuildable
    {
        private readonly ILogger logger;

        public AsyncConsumerEndpoint(ILifetimeScope endpointScope, ILogger logger)
            : base(endpointScope, logger)
        {
            this.logger = logger;
        }

        protected override void ProcessMesage(IMessage e, ILifetimeScope scope)
        {
            bool processed = false;
            if (e is RebuildStarted)
            {
                processed = true;
                OnRebuildStarted(scope);
            }

            if (e is RebuildFinished)
            {
                processed = true;
                OnRebuildFinished(scope);
            }

            if (!processed)
            {
                base.ProcessMesage(e, scope);
            }
                
            
        }

        #region projection rebuild stuff

        private void OnRebuildStarted(ILifetimeScope scope)
        {
            var handler = scope.Resolve<TConsumer>();
            if (handler is IProjection)
            {
                try
                {
                    (handler as IProjection).OnRebuildStarted();
                }
                catch (Exception ex)
                {
                    logger.Error(string.Format("Error during OnRebuildStarted() in projection {0}: {1}", typeof(TConsumer).Name, ex.Message), ex);
                }
            }
            
        }

        private void OnRebuildFinished(ILifetimeScope scope)
        {
            var handler = scope.Resolve<TConsumer>();
            if (handler is IProjection)
            {
                try
                {
                    (handler as IProjection).OnRebuildFinished();
                }
                catch (Exception ex)
                {
                    logger.Error(string.Format("Error during OnRebuildFinished() in projection {0}: {1}", typeof(TConsumer).Name, ex.Message), ex);
                }

            }
            
        }

        public void Rebuild(IEvent @event, ILifetimeScope scope)
        {
            ProcessMesage(@event, scope);
        }

        #endregion
    }
}
