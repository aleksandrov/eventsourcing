﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using EventSourcing.Cqrs.Messages;
using EventSourcing.Interfaces;

namespace EventSourcing.EventDispatcher.Endpoints
{
    class ActionCache<THandler>
    {
        private readonly IDictionary<Type, Func<THandler, IMessage, ExecutionResult>> callsCache;
        private readonly object lockObject;

        public ActionCache()
        {
            lockObject = new object();
            callsCache = new Dictionary<Type, Func<THandler, IMessage, ExecutionResult>>();
        }

        public Func<THandler, IMessage, ExecutionResult> GetCallAction(Type concreteMessageType)
        {
            lock (lockObject)
            {
                if (callsCache.ContainsKey(concreteMessageType))
                {
                    return callsCache[concreteMessageType];
                }

                var action = BuildCall(concreteMessageType);
                callsCache.Add(concreteMessageType, action);
                return action;
            }
        }

        internal static Func<THandler, IMessage, ExecutionResult> BuildCall(Type concreteMessageType)
        {
            Type concreteHandlesType = typeof(IHandles<>).MakeGenericType(concreteMessageType);
            Type concreteConsumeType = typeof(IConsume<>).MakeGenericType(concreteMessageType);

            //if (typeof(THandler).IsAssignableFrom(concreteHandlesType))
            if (concreteHandlesType.IsAssignableFrom(typeof(THandler)))
            {
                // CommandHandler or SagaHandler
                return BuildHandleCallInternal(concreteHandlesType, concreteMessageType);
            }

            //if (typeof(THandler).IsAssignableFrom(concreteConsumeType))
            if (concreteConsumeType.IsAssignableFrom(typeof(THandler)))
            {
                // Projection of Async CommandHandler
                var consume = BuildConsumeCallInternal(concreteConsumeType, concreteMessageType);
                return (target, message) =>
                {
                    consume(target, message);
                    return new ExecutionResult();
                };
            }

            // Handler does not support the message.
            return (target, message) => new ExecutionResult();
        }

        private static Func<THandler, IMessage, ExecutionResult> BuildHandleCallInternal(Type concreteConsumerType, Type concreteMessageType)
        {
            var method = concreteConsumerType.GetMethods().Single();

            var instance = Expression.Parameter(typeof(THandler), "instance");
            var value = Expression.Parameter(typeof(IMessage), "message");

            UnaryExpression instanceCasted = (!typeof(THandler).IsValueType)
                                                 ? Expression.TypeAs(instance, concreteConsumerType)
                                                 : Expression.Convert(instance, concreteConsumerType);

            UnaryExpression valueCasted = (!concreteMessageType.IsValueType)
                                              ? Expression.TypeAs(value, concreteMessageType)
                                              : Expression.Convert(value, concreteMessageType);


            return Expression.Lambda<Func<THandler, IMessage, ExecutionResult>>(Expression.Call(instanceCasted, method, new[] { valueCasted }), new[] { instance, value }).Compile();
        }

        private static Action<THandler, IMessage> BuildConsumeCallInternal(Type concreteConsumerType, Type concreteMessageType)
        {
            var method = concreteConsumerType.GetMethods().Single();

            var instance = Expression.Parameter(typeof(THandler), "instance");
            var value = Expression.Parameter(typeof(IMessage), "message");

            UnaryExpression instanceCasted = (!typeof(THandler).IsValueType)
                                                 ? Expression.TypeAs(instance, concreteConsumerType)
                                                 : Expression.Convert(instance, concreteConsumerType);

            UnaryExpression valueCasted = (!concreteMessageType.IsValueType)
                                              ? Expression.TypeAs(value, concreteMessageType)
                                              : Expression.Convert(value, concreteMessageType);


            return Expression.Lambda<Action<THandler, IMessage>>(Expression.Call(instanceCasted, method, new[] { valueCasted }), new[] { instance, value }).Compile();
        }
    }
}
