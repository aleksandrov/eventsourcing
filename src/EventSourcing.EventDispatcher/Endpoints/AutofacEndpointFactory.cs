﻿using System;
using Autofac;
using EventSourcing.EventDispatcher.Endpoints.Async;
using EventSourcing.EventDispatcher.Subscribers.Subscriptions;

namespace EventSourcing.EventDispatcher.Endpoints
{
    public class AutofacEndpointFactory : IEndpointFactory
    {
        private readonly ILifetimeScope scope;

        public AutofacEndpointFactory(ILifetimeScope scope)
        {
            if (scope == null) throw new ArgumentNullException("scope");
            this.scope = scope;
        }

        public IEndpoint CreateProjectionEndpoint(EventSubscription subscription)
        {
            try
            {
                Type handlerType = typeof (AsyncConsumerEndpoint<>).MakeGenericType(subscription.SubscriberType);

                object result;
                if (!scope.TryResolve(handlerType, out result))
                {
                    result = scope.ResolveNamed(EventDispatcherModule.COMMAND_ENDPOINT_KEY, handlerType);
                }

                return (IEndpoint)result;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(string.Format("Unable to create endpoint for {0}", subscription.SubscriberType), ex);
            }
        }

        public IEndpoint CreateCommandHandlerEndpoint(CommandSubscription subscription)
        {
            try
            {
                Type handlerType = null;
                if (subscription.IsAsync)
                {
                    handlerType = typeof (AsyncConsumerEndpoint<>).MakeGenericType(subscription.SubscriberType);
                }
                else
                {
                    handlerType = typeof (CommandHandlerEndpoint<>).MakeGenericType(subscription.SubscriberType);
                }

                object result;
                if (!scope.TryResolve(handlerType, out result))
                {
                    result = scope.ResolveNamed(EventDispatcherModule.COMMAND_ENDPOINT_KEY, handlerType);
                }

                return (IEndpoint)result;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(string.Format("Unable to create endpoint for {0}", subscription.SubscriberType), ex);
            }
        }

        public IEndpoint CreateSagaHandlerEndpoint(SagaSubscription subscription)
        {
            try
            {
                Type handlerType = null;
                if (subscription.IsAsync)
                {
                    handlerType = typeof(SagaEndpointBase<>).MakeGenericType(subscription.SubscriberType);
                }
                else
                {
                    handlerType = typeof(SagaEndpointBase<>).MakeGenericType(subscription.SubscriberType);
                }

                object result;
                if (!scope.TryResolve(handlerType, out result))
                {
                    result = scope.ResolveNamed(EventDispatcherModule.COMMAND_ENDPOINT_KEY, handlerType);
                }

                return (IEndpoint)result;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(string.Format("Unable to create endpoint for {0}", subscription.SubscriberType), ex);
            }
        }
    }
}
