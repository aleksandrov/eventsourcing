﻿using EventSourcing.EventDispatcher.Subscribers.Subscriptions;

namespace EventSourcing.EventDispatcher.Endpoints
{
    public interface IEndpointFactory
    {
        IEndpoint CreateProjectionEndpoint(EventSubscription subscription);
        IEndpoint CreateCommandHandlerEndpoint(CommandSubscription subscription);
        IEndpoint CreateSagaHandlerEndpoint(SagaSubscription subscription);
    }
}
