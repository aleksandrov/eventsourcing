﻿using Autofac;
using EventSourcing.Cqrs.Messages;
using EventSourcing.Cqrs.Messages.Commands;
using EventSourcing.Interfaces.Services;

namespace EventSourcing.EventDispatcher.Endpoints
{
    public class CommandHandlerEndpoint<THandler> : IEndpoint
    {
        private readonly ILogger logger;
        private readonly ActionCache<THandler> actionCache;

        public CommandHandlerEndpoint(ILogger logger)
        {
            this.logger = logger;
            actionCache = new ActionCache<THandler>();
        }

        public virtual ExecutionResult Handle(IMessage e, ILifetimeScope scope )
        {
            var handler = scope.Resolve<THandler>();

            var action = actionCache.GetCallAction(e.GetType());
            var result = action(handler, e);
            if (e is ICommand)
            {
                var cmd = e as ICommand;
                logger.DebugFormat("Message {0} handled by {1}. Command Id: {2}", e.GetType().Name, handler.GetType().Name, cmd.Id);
            }
            else
            {
                logger.DebugFormat("Message {0} handled by {1}", e.GetType().Name, handler.GetType().Name);    
            }

                
            return result;
        }

        public string Name
        {
            get { return typeof (THandler).Name; }
        }
    }
}
