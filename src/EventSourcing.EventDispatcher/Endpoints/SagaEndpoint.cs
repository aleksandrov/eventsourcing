﻿using Autofac;
using EventSourcing.EventDispatcher.Endpoints.Async;
using EventSourcing.Interfaces.Services;

namespace EventSourcing.EventDispatcher.Endpoints
{
    public class SagaEndpointBase<THandler> : AsyncEndpointBase<THandler>
    {
        public SagaEndpointBase(ILifetimeScope endpointScope, ILogger logger) : base(endpointScope, logger)
        {
        }
    }
}
