﻿using Autofac;
using EventSourcing.Cqrs.Messages;

namespace EventSourcing.EventDispatcher.Endpoints
{
    public interface IEndpoint
    {
        ExecutionResult Handle(IMessage e, ILifetimeScope scope);

        string Name { get; }
    }
}
