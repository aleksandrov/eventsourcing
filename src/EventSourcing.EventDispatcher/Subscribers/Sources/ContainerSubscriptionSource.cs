﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using Autofac.Core;
using EventSourcing.Cqrs.Messages.Commands;
using EventSourcing.EventDispatcher.Subscribers.Subscriptions;
using EventSourcing.Interfaces;

namespace EventSourcing.EventDispatcher.Subscribers.Sources
{
    public class ContainerSubscriptionSource : ISubscriptionSource
    {
        private readonly ILifetimeScope scope;
        private readonly Type genericConsumeType;
        private readonly Type genericHandleType;


        public ContainerSubscriptionSource(ILifetimeScope scope)
        {
            if (scope == null) throw new ArgumentNullException("scope");
            this.scope = scope;

            genericConsumeType = typeof(IConsume<>);
            genericHandleType = typeof(IHandles<>);

        }

        public IEnumerable<ISubscription> GetSubscriptions()
        {
            var registrations = scope.ComponentRegistry.Registrations
                .SelectMany(r => r.Services, (registration, service) => new { registration, service })
                .Where(pair => pair.service is TypedService)
                .Select(pair => new { Registration = pair.registration, Service = pair.service as TypedService })
                .Where(pair => pair.Service.ServiceType.IsGenericType)
                .Where(pair => pair.Service.ServiceType.GetGenericTypeDefinition() == genericConsumeType || pair.Service.ServiceType.GetGenericTypeDefinition() == genericHandleType)
                .Select(pair =>
                {
                    ISubscription result;
                    if (pair.Service.ServiceType.GetGenericTypeDefinition() == genericConsumeType)
                    {
                        var messageType = pair.Service.ServiceType.GetGenericArguments().Single();
                        if (messageType.IsAssignableTo<ICommand>())
                        {
                            // async command handler
                            result = new CommandSubscription(subscriberType: pair.Registration.Activator.LimitType, eventType: pair.Service.ServiceType.GetGenericArguments()[0], isAsync: true);    
                        }
                        else
                        {
                            // event handler
                            result = new EventSubscription(subscriberType: pair.Registration.Activator.LimitType, eventType: pair.Service.ServiceType.GetGenericArguments()[0]);
                        }
                    }
                    else
                    {
                        var messageType = pair.Service.ServiceType.GetGenericArguments().Single();
                        if (messageType.IsAssignableTo<ICommand>())
                        {
                            // command handler
                            result = new CommandSubscription(subscriberType: pair.Registration.Activator.LimitType, eventType: pair.Service.ServiceType.GetGenericArguments()[0], isAsync: false);
                        }
                        else
                        {
                            // saga handler
                            result = new SagaSubscription(subscriberType: pair.Registration.Activator.LimitType, eventType: pair.Service.ServiceType.GetGenericArguments()[0]);
                        }

                    }

                    return result;
                });

            return registrations;
        }
    }
}
