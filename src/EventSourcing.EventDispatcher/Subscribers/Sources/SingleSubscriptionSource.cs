﻿using System.Collections.Generic;

namespace EventSourcing.EventDispatcher.Subscribers.Sources
{
    class SingleSubscriptionSource : ISubscriptionSource
    {
        private readonly IList<ISubscription> subscriptions;

        public SingleSubscriptionSource(ISubscription subscription)
        {
            subscriptions = new List<ISubscription>();
            subscriptions.Add(subscription);
        }

        public IEnumerable<ISubscription> GetSubscriptions()
        {
            return subscriptions;
        }
    }
}
