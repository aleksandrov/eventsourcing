﻿using System;
using System.Collections.Generic;
using System.Linq;
using EventSourcing.EventDispatcher.Subscribers.Subscriptions;
using EventSourcing.Interfaces;

namespace EventSourcing.EventDispatcher.Subscribers.Sources
{
    class AppDomainSubscriptionSource : ISubscriptionSource
    {
        private readonly AppDomain domain;
        private readonly Func<Type, bool> typeFilter;
        private readonly Type genericConsumeType;
        private readonly Type genericHandleType;

        public AppDomainSubscriptionSource(AppDomain domain, Func<Type, bool> typeFilter)
        {
            if (domain == null) throw new ArgumentNullException("domain");
            if (typeFilter == null) throw new ArgumentNullException("typeFilter");
            this.domain = domain;
            this.typeFilter = typeFilter;

            genericConsumeType = typeof(IConsume<>);
            genericHandleType = typeof(IHandles<>);
        }

        public IEnumerable<ISubscription> GetSubscriptions()
        {
            var types = domain.GetAssemblies().SelectMany(a => a.GetTypes())
                .Where(t => typeFilter(t));
/*
                .Where(t => t.Namespace != null && t.Namespace.StartsWith("Crm"))
                .Where(t => !t.IsInterface && !t.IsAbstract)
*/

            return types.SelectMany(t => t.GetInterfaces(), (t, i) => new {t, i})
                     .Where(pair => pair.i.IsGenericType)
                     .Where(pair => pair.i.GetGenericTypeDefinition() == genericConsumeType || pair.i.GetGenericTypeDefinition() == genericHandleType)
                     .Select(pair =>
                         {
                             ISubscription result;
                             if (pair.i.GetGenericTypeDefinition() == genericConsumeType)
                             {
                                 result = new EventSubscription(subscriberType: pair.t, eventType: pair.i.GetGenericArguments()[0]);
                             }
                             else
                             {
                                 result = new CommandSubscription(subscriberType: pair.t, eventType: pair.i.GetGenericArguments()[0]);
                             }

                             return result;
                         });
        }
    }
}
