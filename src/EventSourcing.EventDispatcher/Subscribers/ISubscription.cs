﻿using System;
using EventSourcing.EventDispatcher.Endpoints;

namespace EventSourcing.EventDispatcher.Subscribers
{
    public interface ISubscription
    {
        string GetId();

        Type EventType { get; }

        bool IsAsync { get; }

        IEndpoint CreateEndpoint(IEndpointFactory factory);
    }
}
