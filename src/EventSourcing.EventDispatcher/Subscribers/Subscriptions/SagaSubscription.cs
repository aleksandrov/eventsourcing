﻿using System;
using EventSourcing.EventDispatcher.Endpoints;

namespace EventSourcing.EventDispatcher.Subscribers.Subscriptions
{
    public class SagaSubscription : TypedSubscriptionBase
    {
        public SagaSubscription(Type subscriberType, Type eventType) : base(subscriberType, eventType)
        {
            IsAsync = false;
        }

        public override IEndpoint CreateEndpoint(IEndpointFactory factory)
        {
            return factory.CreateSagaHandlerEndpoint(this);
        }
    }
}
