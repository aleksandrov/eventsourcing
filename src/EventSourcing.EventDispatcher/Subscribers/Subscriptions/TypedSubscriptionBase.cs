﻿using System;
using EventSourcing.EventDispatcher.Endpoints;

namespace EventSourcing.EventDispatcher.Subscribers.Subscriptions
{
    public abstract class TypedSubscriptionBase : ISubscription
    {
        protected TypedSubscriptionBase(Type subscriberType, Type eventType)
        {
            SubscriberType = subscriberType;
            EventType = eventType;
        }

        public virtual string GetId()
        {
            if (IsAsync)
            {
                return "Async" + SubscriberType.Name;
            }

            return SubscriberType.Name;
        }

        public Type SubscriberType { get; private set; }

        public Type EventType { get; private set; }

        public bool IsAsync { get; protected set; }

        public abstract IEndpoint CreateEndpoint(IEndpointFactory factory);
    }
}
