﻿using System;
using EventSourcing.EventDispatcher.Endpoints;

namespace EventSourcing.EventDispatcher.Subscribers.Subscriptions
{
    public class CommandSubscription : TypedSubscriptionBase
    {
        public CommandSubscription(Type subscriberType, Type eventType, bool isAsync = false) : base(subscriberType, eventType)
        {
            IsAsync = isAsync;
        }

        public override IEndpoint CreateEndpoint(IEndpointFactory factory)
        {
            return factory.CreateCommandHandlerEndpoint(this);
        }
    }
}
