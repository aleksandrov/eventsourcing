﻿using System;
using EventSourcing.EventDispatcher.Endpoints;

namespace EventSourcing.EventDispatcher.Subscribers.Subscriptions
{
    public class EventSubscription : TypedSubscriptionBase
    {
        public EventSubscription(Type subscriberType, Type eventType) : base(subscriberType, eventType)
        {
            IsAsync = true;
        }

        public override IEndpoint CreateEndpoint(IEndpointFactory factory)
        {
            return factory.CreateProjectionEndpoint(this);
        }
    }
}
