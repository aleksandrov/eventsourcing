﻿using System.Collections.Generic;

namespace EventSourcing.EventDispatcher.Subscribers
{
    public interface ISubscriptionSource
    {
        IEnumerable<ISubscription> GetSubscriptions();
    }
}
