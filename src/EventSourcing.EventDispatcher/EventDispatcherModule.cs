﻿using Autofac;
using EventSourcing.EventDispatcher.Endpoints;
using EventSourcing.EventDispatcher.Endpoints.Async;
using EventSourcing.EventDispatcher.Routing;
using EventSourcing.EventDispatcher.Subscribers.Sources;

namespace EventSourcing.EventDispatcher
{
    public class EventDispatcherModule : Module
    {
        public const string COMMAND_ENDPOINT_KEY = "main";

        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<RoutingTable>().As<IRoutingTable>().SingleInstance();

            builder.RegisterType<AutofacEndpointFactory>().AsImplementedInterfaces();

            builder.RegisterGeneric(typeof(CommandHandlerEndpoint<>)).Named(COMMAND_ENDPOINT_KEY, typeof(CommandHandlerEndpoint<>));
            builder.RegisterGeneric(typeof(SagaEndpointBase<>)).Named(COMMAND_ENDPOINT_KEY, typeof(SagaEndpointBase<>));
            builder.RegisterGeneric(typeof(AsyncConsumerEndpoint<>)).Named(COMMAND_ENDPOINT_KEY, typeof(AsyncConsumerEndpoint<>));

            builder.RegisterType<ContainerSubscriptionSource>().AsImplementedInterfaces();
            //new AppDomainSubscriptionSource(AppDomain.CurrentDomain, t => !t.IsInterface && !t.IsAbstract && t.Namespace != null && t.Namespace.StartsWith("Crm")),

            builder.RegisterType<EventDispatcher>().AsSelf().AsImplementedInterfaces().SingleInstance();
        }
    }
}
