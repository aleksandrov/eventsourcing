﻿using Autofac;
using EventSourcing.Cqrs.Handlers;
using EventSourcing.Cqrs.Services;
using EventSourcing.Interfaces;
using EventSourcing.Interfaces.Services;

namespace EventSourcing.Cqrs
{
    public class CqrsModule : Module
    {


        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<NullLogger>().As<ILogger>();

            builder.RegisterType<HeaderManager>().As<IHeaderManager>();
            builder.RegisterType<CqrsHeaderProcessor>().As<IHeaderProcessor>();
            builder.RegisterType<RuntimeManager>().As<IRuntimeManager>().SingleInstance();
            builder.RegisterType<SystemHandlers>().AsSelf().AsImplementedInterfaces();
        }
    }
}
