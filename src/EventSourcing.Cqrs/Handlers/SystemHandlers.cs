﻿using EventSourcing.Cqrs.Messages;
using EventSourcing.Cqrs.Messages.Commands;
using EventSourcing.Cqrs.Messages.Commands.Runtime;
using EventSourcing.Cqrs.Services;
using EventSourcing.Interfaces;

namespace EventSourcing.Cqrs.Handlers
{
	public class SystemHandlers :
		IHandles<StartCommand>,
        IHandles<StopCommand>,
	    IHandles<EchoCommand>
	{
		private readonly IRuntimeManager runtimeManager;

	    public SystemHandlers(IRuntimeManager runtimeManager)
		{
			this.runtimeManager = runtimeManager;
		}

        public ExecutionResult Handle(StartCommand message)
		{
			runtimeManager.Start();

            return ExecutionResult.Empty;
		}

        public ExecutionResult Handle(EchoCommand message)
	    {
	        runtimeManager.Echo(message.Message);

            return ExecutionResult.Empty;
	    }

	    public ExecutionResult Handle(StopCommand message)
	    {
	        runtimeManager.Stop();

            return ExecutionResult.Empty;
	    }
	}
}