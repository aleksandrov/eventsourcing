﻿using System;
using System.Linq;
using EventSourcing.Cqrs.Messages.Events.Runtime;
using EventSourcing.Interfaces.Services;

namespace EventSourcing.Cqrs.Services
{
    class RuntimeManager : IRuntimeManager
    {
        private readonly IEventPublisher eventPublisher;
        private readonly ILogger logger;

        private bool isStopping;

        public RuntimeManager(IEventPublisher eventPublisher, ILogger logger)
        {
            this.eventPublisher = eventPublisher;
            this.logger = logger;
        }

        public void Start()
        {
            eventPublisher.Publish(new SystemStartedEvent());
        }

        public void Stop()
        {
            if (!isStopping)
            {
                eventPublisher.Publish(new SystemStoppingEvent());
                isStopping = true;
            }
        }

        public void Echo(string message)
        {
            if (string.IsNullOrEmpty(message))
            {
                logger.DebugFormat("Ohce");
            }
            else
            {
                logger.InfoFormat("Ohce: {0}", new String(message.Reverse().ToArray()));    
            }
            
        }

    }
}