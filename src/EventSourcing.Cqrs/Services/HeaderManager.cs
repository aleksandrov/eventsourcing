﻿using System.Collections.Generic;
using System.Linq;
using EventSourcing.Cqrs.Messages;
using EventSourcing.Cqrs.Messages.Events;
using EventSourcing.Interfaces;
using EventSourcing.Interfaces.Services;

namespace EventSourcing.Cqrs.Services
{
    class HeaderManager : IHeaderManager
    {
        private readonly IEnumerable<IHeaderProcessor> _processors;

        public HeaderManager(IEnumerable<IHeaderProcessor> processors)
        {
            _processors = processors;
        }

        public void UpdateHeaders(IDictionary<string, object> headers, IMessage message)
        {
            var calculatedHeaders = _processors.SelectMany(p => p.GetHeaders(message));

            foreach (var header in calculatedHeaders)
            {
                if (headers.ContainsKey(header.Key))
                {
                    headers[header.Key] = header.Value;
                }
                else
                {
                    headers.Add(header.Key, header.Value);
                }
            }
        }

        public void UpdateEvent(IEvent message, IDictionary<string, object> headers)
        {
            foreach (var processor in _processors)
            {
                processor.UpdateEvent(message, headers);
            }
        }
    }
}
