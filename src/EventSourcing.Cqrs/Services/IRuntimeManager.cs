﻿namespace EventSourcing.Cqrs.Services
{
	public interface IRuntimeManager
	{
		void Start();
	    void Stop();
	    void Echo(string message);
	}
}