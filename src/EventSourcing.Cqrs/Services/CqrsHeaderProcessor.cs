﻿using System;
using System.Collections.Generic;
using System.Linq;
using EventSourcing.Cqrs.Messages;
using EventSourcing.Cqrs.Messages.Commands;
using EventSourcing.Cqrs.Messages.Events;
using EventSourcing.Interfaces;

namespace EventSourcing.Cqrs.Services
{
    class CqrsHeaderProcessor : IHeaderProcessor
    {
        public const string TIMESTAMP_HEADER = "Timestamp";
        public const string CORRELATION_HEADER = "CorrelationId";
        public const string USER_HEADER = "User";
        public const string REQUEST_ID_HEADER = "RequestId";

        public IDictionary<string, object> GetHeaders(IMessage message)
        {
            var headers = new Dictionary<string, object>();

            // timestamp header
            SetTimestamp(headers);
            SetCorrelationId(headers, message);
            SetRequestId(headers, message);

            // set tenant values if any
            if (message is AuthorizedCommand)
            {
                var tenantCmd = message as AuthorizedCommand;
                headers.Add(USER_HEADER, tenantCmd.AuthorizedUserId);
            }

            return headers;

        }

        public void UpdateEvent(IEvent message, IDictionary<string, object> headers)
        {
            message.Created = GetHeader<DateTime>(headers, TIMESTAMP_HEADER);


            var correlationId = GetHeader<Guid>(headers, CORRELATION_HEADER);
            if (message.CorrelationId == Guid.Empty)
            {
                message.CorrelationId = correlationId;
            }


            var userId = GetHeader<Guid>(headers, USER_HEADER);
            if (message is AuthorizedEvent && (message as AuthorizedEvent).AuthorizedUserId == Guid.Empty)
            {
                (message as AuthorizedEvent).AuthorizedUserId = userId;
            }
        }

        private static void SetRequestId(IDictionary<string, object> headers, IMessage message)
        {
            if (message is ICommand)
            {
                headers.Add(REQUEST_ID_HEADER, (message as ICommand).Id);    
            }
            
        }


        private static void SetCorrelationId(IDictionary<string, object> headers, IMessage message)
        {
            headers.Add(CORRELATION_HEADER, message.CorrelationId);
        }

        private static void SetTimestamp(IDictionary<string, object> headers)
        {
            headers.Add(TIMESTAMP_HEADER, DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc));
        }

        private static T GetHeader<T>(IDictionary<string, object> headers, string name)
        {
            var value = headers.Where(h => h.Key == name).Select(h => h.Value).FirstOrDefault();
            if (value != null)
            {
                return (T)value;
            }
            return default(T);
        }

    }
}
