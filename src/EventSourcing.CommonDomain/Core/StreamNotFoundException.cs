﻿using System;
using System.Runtime.Serialization;

namespace EventSourcing.CommonDomain.Core
{
    public class StreamNotFoundException : Exception
    {
		public StreamNotFoundException()
		{}

		public StreamNotFoundException(string message)
			: base(message)
		{}

		public StreamNotFoundException(string message, Exception innerException)
			: base(message, innerException)
		{}

        public StreamNotFoundException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{}
    }
}
