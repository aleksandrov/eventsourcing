set VERSION=1.0.0.30

cd output\EventSourcing.Cqrs.Messages
..\..\.nuget\NuGet.exe push EventSourcing.Cqrs.Messages.%VERSION%.nupkg
..\..\.nuget\NuGet.exe delete EventSourcing.Cqrs.Messages %VERSION% -NoPrompt

cd ..\..\
cd output\EventSourcing.Interfaces
..\..\.nuget\NuGet.exe push EventSourcing.Interfaces.%VERSION%.nupkg
..\..\.nuget\NuGet.exe delete EventSourcing.Interfaces %VERSION% -NoPrompt

cd ..\..\
cd output\EventSourcing.CommonDomain
..\..\.nuget\NuGet.exe push EventSourcing.CommonDomain.%VERSION%.nupkg
..\..\.nuget\NuGet.exe delete EventSourcing.CommonDomain %VERSION% -NoPrompt

cd ..\..\
cd output\EventSourcing.Cqrs
..\..\.nuget\NuGet.exe push EventSourcing.Cqrs.%VERSION%.nupkg
..\..\.nuget\NuGet.exe delete EventSourcing.Cqrs %VERSION% -NoPrompt

cd ..\..\
cd output\EventSourcing.Cqrs.Autofac
..\..\.nuget\NuGet.exe push EventSourcing.Cqrs.Autofac.%VERSION%.nupkg
..\..\.nuget\NuGet.exe delete EventSourcing.Cqrs.Autofac %VERSION% -NoPrompt

cd ..\..\
cd output\EventSourcing.EventDispatcher
..\..\.nuget\NuGet.exe push EventSourcing.EventDispatcher.%VERSION%.nupkg
..\..\.nuget\NuGet.exe delete EventSourcing.EventDispatcher %VERSION% -NoPrompt

cd ..\..\
cd output\EventSourcing.EventStore
..\..\.nuget\NuGet.exe push EventSourcing.EventStore.%VERSION%.nupkg
..\..\.nuget\NuGet.exe delete EventSourcing.EventStore %VERSION% -NoPrompt

cd ..\..\
cd output\EventSourcing.Rebuild.Messages
..\..\.nuget\NuGet.exe push EventSourcing.Rebuild.Messages.%VERSION%.nupkg
..\..\.nuget\NuGet.exe delete EventSourcing.Rebuild.Messages %VERSION% -NoPrompt

cd ..\..\
cd output\EventSourcing.Rebuild
..\..\.nuget\NuGet.exe push EventSourcing.Rebuild.%VERSION%.nupkg
..\..\.nuget\NuGet.exe delete EventSourcing.Rebuild %VERSION% -NoPrompt

cd ..\..\
cd output\EventSourcing.Logging
..\..\.nuget\NuGet.exe push EventSourcing.Logging.%VERSION%.nupkg
..\..\.nuget\NuGet.exe delete EventSourcing.Logging %VERSION% -NoPrompt

cd ..\..\
cd output\EventSourcing.Misc.Mongo
..\..\.nuget\NuGet.exe push EventSourcing.Misc.Mongo.%VERSION%.nupkg
..\..\.nuget\NuGet.exe delete EventSourcing.Misc.Mongo %VERSION% -NoPrompt

