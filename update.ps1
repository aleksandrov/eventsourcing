$libs = "EventSourcing.Interfaces", "EventSourcing.Cqrs.Messages", "EventSourcing.Cqrs", "EventSourcing.Cqrs.Autofac", "EventSourcing.EventDispatcher", "EventSourcing.EventStore", "EventSourcing.Rebuild", "EventSourcing.Logging", "EventSourcing.Misc.Mongo", "EventSourcing.CommonDomain"
foreach ($lib in $libs)
{
      Update-Package $lib -Version 1.0.0.22
}
