set VERSION=1.0.0.30

cd output\EventSourcing.Cqrs.Messages
..\..\.nuget\NuGet.exe pack -Version %VERSION%

cd ..\..\
cd output\EventSourcing.Interfaces
..\..\.nuget\NuGet.exe pack -Version %VERSION%

cd ..\..\
cd output\EventSourcing.CommonDomain
..\..\.nuget\NuGet.exe pack -Version %VERSION%

cd ..\..\
cd output\EventSourcing.Cqrs
..\..\.nuget\NuGet.exe pack -Version %VERSION%

cd ..\..\
cd output\EventSourcing.Cqrs.Autofac
..\..\.nuget\NuGet.exe pack -Version %VERSION%

cd ..\..\
cd output\EventSourcing.EventDispatcher
..\..\.nuget\NuGet.exe pack -Version %VERSION%

cd ..\..\
cd output\EventSourcing.EventStore
..\..\.nuget\NuGet.exe pack -Version %VERSION%

cd ..\..\
cd output\EventSourcing.Rebuild.Messages
..\..\.nuget\NuGet.exe pack -Version %VERSION%

cd ..\..\
cd output\EventSourcing.Rebuild
..\..\.nuget\NuGet.exe pack -Version %VERSION%

cd ..\..\
cd output\EventSourcing.Logging
..\..\.nuget\NuGet.exe pack -Version %VERSION%

cd ..\..\
cd output\EventSourcing.Misc.Mongo
..\..\.nuget\NuGet.exe pack -Version %VERSION%
